// go version: go1.13 darwin/amd64
// OS: macOS Catalina

package raft

//
// this is an outline of the API that raft must expose to
// the service (or tester). see comments below for
// each of these functions for more details.
//
// rf = Make(...)
//   create a new Raft server.
// rf.Start(command interface{}) (index, term, isleader)
//   start agreement on a new log entry
// rf.GetState() (term, isLeader)
//   ask a Raft for its current term, and whether it thinks it is leader
// ApplyMsg
//   each time a new entry is committed to the log, each Raft peer
//   should send an ApplyMsg to the service (or tester)
//   in the same server.
//

import (
	"math/rand"
	"sync"
	"time"
)
import "sync/atomic"
import "mp2/src/labrpc"

const (
	Leader             string        = "Leader"
	Candidate          string        = "Candidate"
	Follower           string        = "Follower"
	BaseTime           int           = 150
	RandomizedTime     int           = 450
	ChannelBufferSize  int           = 100
	LeaderTimeoutValue time.Duration = 100
)

// ApplyMsg
// as each Raft peer becomes aware that successive log entries are
// committed, the peer should send an ApplyMsg to the service (or
// tester) on the same server, via the applyCh passed to Make(). set
// CommandValid to true to indicate that the ApplyMsg contains a newly
// committed log entry.
//
type ApplyMsg struct {
	CommandValid bool
	Command      interface{}
	CommandIndex int
}

// Log
// As defined in slides
type Log struct {
	Command interface{}
	Term    int
}

// Raft
// A Go object implementing a single Raft peer.
//
type Raft struct {
	mu    sync.Mutex          // Lock to protect shared access to this peer's state
	peers []*labrpc.ClientEnd // RPC end points of all peers
	me    int                 // this peer's index into peers[]
	dead  int32               // set by Kill()

	// Your data here (2A, 2B).
	// Look at the paper's Figure 2 for a description of what
	// state a Raft server must maintain.
	// You may also need to add other state, as per your implementation.

	// persistent state
	currentTerm  int
	currentState string
	votedFor     int
	voteReceived int
	logs         []Log

	// volatile state on all servers
	commitIndex int
	lastApplied int

	// volatile state on leaders
	nextIndex  []int
	matchIndex []int

	// Channels used for communication between peers
	winElectionChannel chan bool
	voteGrantedChannel chan bool
	applyChannel       chan ApplyMsg
	heartbeatChannel   chan bool
}

// GetState
// return currentTerm and whether this server
// believes it is the leader.
func (rf *Raft) GetState() (int, bool) {
	//fmt.Printf("[GetState] - DEADLOCK DETECTOR [Enter %d] T: %d\n", rf.me, rf.currentTerm)

	var term int
	var isleader bool

	// Your code here (2A).
	rf.mu.Lock()
	defer rf.mu.Unlock()
	term = rf.currentTerm
	isleader = rf.currentState == Leader
	return term, isleader
}

// RequestVoteArgs
// example RequestVote RPC arguments structure.
// field names must start with capital letters!
//
type RequestVoteArgs struct {
	// Your data here (2A, 2B).
	Term         int
	CandidateId  int
	LastLogIndex int
	LastLogTerm  int
}

// RequestVoteReply
// example RequestVote RPC reply structure.
// field names must start with capital letters!
//
type RequestVoteReply struct {
	// Your data here (2A).
	Term        int
	VoteGranted bool
}

type AppendEntriesArgs struct {
	Term         int
	LeaderID     int
	PrevLogIndex int
	PrevLogTerm  int
	Entries      []Log
	CommitIndex  int
}

type AppendEntriesReply struct {
	Term      int
	Success   bool
	NextIndex int
}

func (rf *Raft) stepDownToFollowerState(term int) {
	//fmt.Printf("[stepDownToFollowerState] - DEADLOCK DETECTOR [Enter %d] T: %d\n", rf.me, rf.currentTerm)
	rf.currentState = Follower
	rf.currentTerm = term
	rf.votedFor = -1
	//fmt.Printf("[stepDownToFollowerState] - DEADLOCK DETECTOR [Exit %d] T: %d\n", rf.me, rf.currentTerm)
}

func (rf *Raft) getLastIndex() int {
	return len(rf.logs) - 1
}

func (rf *Raft) getLastTerm() int {
	return rf.logs[rf.getLastIndex()].Term
}

func (rf *Raft) isMostUpdateLog(args *RequestVoteArgs) bool {
	var candidateTerm, candidateIndex = args.LastLogTerm, args.LastLogIndex
	var currentTerm, currentIndex = rf.getLastTerm(), rf.getLastIndex()
	return candidateTerm > currentTerm || (candidateTerm == currentTerm && candidateIndex >= currentIndex)
}

// RequestVote
// example RequestVote RPC handler.
//
func (rf *Raft) RequestVote(args *RequestVoteArgs, reply *RequestVoteReply) {
	// Reference: https://www.youtube.com/watch?v=YbZ3zDzDnrw

	//fmt.Printf("[RequestVote] - DEADLOCK DETECTOR [Enter %d] T: %d\n", rf.me, rf.currentTerm)

	// Your code here (2A, 2B).
	// Read the fields in "args",
	// and accordingly assign the values for fields in "reply".

	rf.mu.Lock()
	defer rf.mu.Unlock()
	// if term > currentTerm, currentTerm <- term (step down if leader or candidate)
	if rf.currentTerm > args.Term {
		reply.Term = rf.currentTerm
		reply.VoteGranted = false
		return
	}

	// step down to follower if rf.currentTerm < args.Term
	if rf.currentTerm < args.Term {
		rf.stepDownToFollowerState(args.Term)
	}

	reply.Term = rf.currentTerm

	// If term == currentTerm, votedFor is null or candidateId, and candidate's log is at least as complete as local log,
	// 	grant vote and reset election timeout

	if (rf.votedFor == -1 || rf.votedFor == args.CandidateId) && rf.isMostUpdateLog(args) {
		reply.VoteGranted = true
		rf.votedFor = args.CandidateId
		rf.voteGrantedChannel <- true
	} else {
		reply.VoteGranted = false
	}
	//fmt.Printf("[RequestVote] - DEADLOCK DETECTOR [Exit %d] T: %d\n", rf.me, rf.currentTerm)
}

//
// example code to send a RequestVote RPC to a server.
// server is the index of the target server in rf.peers[].
// expects RPC arguments in args.
// fills in *reply with RPC reply, so caller should
// pass &reply.
// the types of the args and reply passed to Call() must be
// the same as the types of the arguments declared in the
// handler function (including whether they are pointers).
//
// The labrpc package simulates a lossy network, in which servers
// may be unreachable, and in which requests and replies may be lost.
// Call() sends a request and waits for a reply. If a reply arrives
// within a timeout interval, Call() returns true; otherwise
// Call() returns false. Thus Call() may not return for a while.
// A false return can be caused by a dead server, a live server that
// can't be reached, a lost request, or a lost reply.
//
// Call() is guaranteed to return (perhaps after a delay) *except* if the
// handler function on the server side does not return.  Thus there
// is no need to implement your own timeouts around Call().
//
// look at the comments in ../labrpc/labrpc.go for more details.
//
// if you're having trouble getting RPC to work, check that you've
// capitalized all field names in structs passed over RPC, and
// that the caller passes the address of the reply struct with &, not
// the struct itself.
//
func (rf *Raft) sendRequestVote(server int, args *RequestVoteArgs, reply *RequestVoteReply) bool {
	ok := rf.peers[server].Call("Raft.RequestVote", args, reply)

	rf.mu.Lock()
	defer rf.mu.Unlock()

	if ok {

		if rf.currentState != Candidate && rf.currentTerm != args.Term {
			return false
		}

		if rf.currentTerm < reply.Term {
			rf.stepDownToFollowerState(reply.Term)
			return false
		}

		if reply.VoteGranted {
			rf.voteReceived += 1
			if rf.voteReceived >= len(rf.peers)/2+1 {
				// win election
				// step up to leader [TODO]
				rf.winElectionChannel <- true
			}
		}

	}
	return ok
}

// Start
// the service using Raft (e.g. a k/v server) wants to start
// agreement on the next command to be appended to Raft's log. if this
// server isn't the leader, returns false. otherwise start the
// agreement and return immediately. there is no guarantee that this
// command will ever be committed to the Raft log, since the leader
// may fail or lose an election. even if the Raft instance has been killed,
// this function should return gracefully.
//
// the first return value is the index that the command will appear at
// if it's ever committed. the second return value is the current
// term. the third return value is true if this server believes it is
// the leader.
//
func (rf *Raft) Start(command interface{}) (int, int, bool) {

	// Your code here (2B).
	rf.mu.Lock()
	defer rf.mu.Unlock()

	index := -1
	term := -1
	isLeader := rf.currentState == Leader

	if isLeader {
		term = rf.currentTerm
		index = rf.getLastIndex() + 1
		rf.logs = append(rf.logs, Log{Term: term, Command: command})
	}

	return index, term, isLeader
}

// Kill
// the tester doesn't halt goroutines created by Raft after each test,
// but it does call the Kill() method. your code can use killed() to
// check whether Kill() has been called. the use of atomic avoids the
// need for a lock.
//
// the issue is that long-running goroutines use memory and may chew
// up CPU time, perhaps causing later tests to fail and generating
// confusing debug output. any goroutine with a long-running loop
// should call killed() to check whether it should stop.
//
func (rf *Raft) Kill() {
	atomic.StoreInt32(&rf.dead, 1)
	// Your code here, if desired.
}

func (rf *Raft) killed() bool {
	z := atomic.LoadInt32(&rf.dead)
	return z == 1
}

func (rf *Raft) getRandomizedTimoutValue(baseTime int, randomizedTime int) time.Duration {
	return time.Millisecond * time.Duration(baseTime+rand.Intn(randomizedTime))
}

func (rf *Raft) changeState(newState string) {
	//fmt.Printf("[changeState] - DEADLOCK DETECTOR [Enter %d] T: %d\n", rf.me, rf.currentTerm)

	rf.mu.Lock()
	defer rf.mu.Unlock()

	rf.currentState = newState

	if newState != Follower {
		// reset channels
		rf.voteGrantedChannel = make(chan bool, ChannelBufferSize)
		rf.winElectionChannel = make(chan bool, ChannelBufferSize)
		rf.heartbeatChannel = make(chan bool, ChannelBufferSize)
	}

	if newState == Leader {
		rf.nextIndex = make([]int, len(rf.peers))
		for _i := range rf.nextIndex {
			rf.nextIndex[_i] = rf.getLastIndex() + 1
		}
		rf.matchIndex = make([]int, len(rf.peers))
	}

	//fmt.Printf("[changeState] - DEADLOCK DETECTOR [Exit %d] T: %d\n", rf.me, rf.currentTerm)

}

func (rf *Raft) setupCandidateState() {
	//fmt.Printf("[setupCandidateState] - DEADLOCK DETECTOR [Enter %d] T: %d\n", rf.me, rf.currentTerm)

	rf.mu.Lock()
	defer rf.mu.Unlock()

	rf.currentState = Candidate
	rf.currentTerm += 1
	rf.voteReceived = 1
	rf.votedFor = rf.me
	//fmt.Printf("[setupCandidateState] - DEADLOCK DETECTOR [Exit %d] T: %d\n", rf.me, rf.currentTerm)

}

func (rf *Raft) broadcastSendAppendEntriesRPC() {
	rf.mu.Lock()
	defer rf.mu.Unlock()

	if rf.currentState == Leader {
		for server := range rf.peers {
			if server != rf.me {
				var args = AppendEntriesArgs{
					/*
						Term         int
						LeaderID     int
						PrevLogIndex int
						PrevLogTerm  int
						Entries      []Log
						CommitIndex  int
					*/
					Term:         rf.currentTerm,
					LeaderID:     rf.me,
					PrevLogIndex: rf.nextIndex[server] - 1,
					PrevLogTerm:  rf.logs[rf.nextIndex[server]-1].Term,
					Entries:      rf.logs[rf.nextIndex[server]:], // all uncommitted messages will be broadcasted
					CommitIndex:  rf.commitIndex,
				}
				//fmt.Printf("[broadcastSendAppendEntriesRPC] - %d is currently the leader and sending append entries request to %d with commitIndex %d...\n", rf.me, server, args.CommitIndex)
				go rf.sendAppendEntries(server, &args, &AppendEntriesReply{})
			}
		}
	}

}

func (rf *Raft) updateIndexInfo(server int, args *AppendEntriesArgs) {
	// todo: double check logic here
	rf.matchIndex[server] = args.PrevLogIndex + len(args.Entries)
	rf.nextIndex[server] = 1 + rf.matchIndex[server]
}

func (rf *Raft) sendAppendEntries(server int, args *AppendEntriesArgs, reply *AppendEntriesReply) bool {
	ok := rf.peers[server].Call("Raft.AppendEntries", args, reply)

	rf.mu.Lock()
	defer rf.mu.Unlock()

	//fmt.Printf("[sendAppendEntries] - ok: %t\n", ok)
	if ok {
		if rf.currentState != Leader || rf.currentTerm != args.Term {
			return false
		}

		if rf.currentTerm < reply.Term {
			rf.stepDownToFollowerState(reply.Term)
			return false
		}

		rf.nextIndex[server] = reply.NextIndex
		if reply.Success {
			rf.updateIndexInfo(server, args)
		}

		// if leader knows that a majority of peers has a specific log, then it can safely apply and commit the log
		for i := rf.getLastIndex(); i >= rf.commitIndex && rf.logs[i].Term == rf.currentTerm; i-- {
			peerContainsThisLogNum := 1
			for peer := range rf.peers {
				if peer != rf.me && rf.matchIndex[peer] >= i {
					peerContainsThisLogNum++
				}
			}

			if peerContainsThisLogNum >= len(rf.peers)/2+1 {
				//fmt.Printf("[sendAppendEntries] - %d is currently changing its commitIndex to %d.\n", rf.me, i)
				rf.commitIndex = i
				go rf.appliedAndCommitLogs()
				return true
			}
		}
	}

	return ok
}

func (rf *Raft) AppendEntries(args *AppendEntriesArgs, reply *AppendEntriesReply) {

	// Reference: https://www.youtube.com/watch?v=YbZ3zDzDnrw
	rf.mu.Lock()
	defer rf.mu.Unlock()

	// if sender's term is older, RPC is rejected, sender reverts to follower and updates its term
	if args.Term < rf.currentTerm {
		reply.Term = rf.currentTerm
		reply.NextIndex = rf.getLastIndex() + 1
		reply.Success = false
		return
	}

	// if receiver's term is older, it reverts to follower, updates its term, then processes RPC normally
	if args.Term > rf.currentTerm {
		rf.stepDownToFollowerState(args.Term)
	}

	// some logs are missing in follower's logs
	if rf.getLastIndex() < args.PrevLogIndex {
		reply.NextIndex = rf.getLastIndex() + 1
		reply.Success = false
		return
	}

	// process RPC

	// todo: refresh heartbeat
	rf.heartbeatChannel <- true
	reply.Term = rf.currentTerm

	// log consistency check 1: check whether the follower's last term matches with the leader's
	var currentTerm = rf.logs[args.PrevLogIndex].Term
	if args.PrevLogIndex >= 0 && args.PrevLogTerm != currentTerm {
		for i := args.PrevLogIndex; i >= 0 && rf.logs[i].Term == currentTerm; i-- {
			reply.NextIndex = i
		}
		reply.Success = false
		return
	}

	// the follower's term matches with the leader's
	// -> logs are safe to merge now (not yet commit btw)

	// eliminate follower's logs that are not conflict with the leader's and obtain the latest log list to be committed
	rf.mergeLogs(args)

	reply.Success = true
	reply.NextIndex = args.PrevLogIndex
	if rf.commitIndex < args.CommitIndex {
		rf.updateCommitIndex(args.CommitIndex)
		go rf.appliedAndCommitLogs()
	}
}

func (rf *Raft) mergeLogs(args *AppendEntriesArgs) {
	localLogPtr := args.PrevLogIndex + 1
	leaderLogPtr := 0
	for ; localLogPtr <= rf.getLastIndex() && leaderLogPtr < len(args.Entries) && rf.logs[localLogPtr].Term == args.Entries[leaderLogPtr].Term; localLogPtr, leaderLogPtr = localLogPtr+1, leaderLogPtr+1 {
		// wait until this for-loop elapse
	}
	// merge valid local log and leader's log
	rf.logs = append(rf.logs[:localLogPtr], args.Entries[leaderLogPtr:]...) // https://go.dev/blog/slices-intro
}

func (rf *Raft) appliedAndCommitLogs() {
	//fmt.Printf("[appliedAndCommitLogs] - DEADLOCK DETECTOR [Enter %d] T: %d\n", rf.me, rf.currentTerm)
	rf.mu.Lock()
	defer rf.mu.Unlock()

	for i := rf.lastApplied + 1; i <= rf.commitIndex; i++ {
		var msg = ApplyMsg{
			//	CommandValid bool
			//	Command      interface{}
			//	CommandIndex int
			CommandValid: true,
			Command:      rf.logs[i].Command,
			CommandIndex: i,
		}
		rf.applyChannel <- msg
		rf.lastApplied += 1
	}
	//fmt.Printf("[appliedAndCommitLogs] - DEADLOCK DETECTOR [Exit %d] T: %d\n", rf.me, rf.currentTerm)
}

func (rf *Raft) updateCommitIndex(leaderCommitIndex int) {
	// rf.commitIndex = min(rf.getLastIndex(), leaderCommitIndex)
	if rf.getLastIndex() < leaderCommitIndex {
		rf.commitIndex = rf.getLastIndex()
	} else {
		rf.commitIndex = leaderCommitIndex
	}
	//fmt.Printf("[updateCommitIndex] - %d is currently changing its commitIndex to %d.\n", rf.me, rf.commitIndex)
}

func (rf *Raft) broadcastSendRequestVoteRPC() {
	//fmt.Printf("[broadcastSendRequestVoteRPC] - DEADLOCK DETECTOR [Enter %d] T: %d\n", rf.me, rf.currentTerm)

	rf.mu.Lock()
	var args = RequestVoteArgs{
		Term:         rf.currentTerm,
		CandidateId:  rf.me,
		LastLogIndex: rf.getLastIndex(), // used for picking up the best leader possible
		LastLogTerm:  rf.getLastTerm(),  // used for picking up the best leader possible
	}
	rf.mu.Unlock()

	for peer := range rf.peers {
		if peer != rf.me {
			go rf.sendRequestVote(peer, &args, &RequestVoteReply{})
		}
	}
	//fmt.Printf("[broadcastSendRequestVoteRPC] - DEADLOCK DETECTOR [Exit %d] T: %d\n", rf.me, rf.currentTerm)

}

func (rf *Raft) run() {
	for !rf.killed() {
		if rf.currentState == Follower {
			select {
			case <-rf.winElectionChannel:
				// do nothing
			case <-rf.heartbeatChannel:
				// do nothing
			case <-rf.voteGrantedChannel:
				// do nothing
			case <-time.After(rf.getRandomizedTimoutValue(BaseTime, RandomizedTime)):
				rf.changeState(Candidate)
			}
		} else if rf.currentState == Leader {
			select {
			case <-time.After(time.Millisecond * LeaderTimeoutValue):
				go rf.broadcastSendAppendEntriesRPC()
			case <-rf.heartbeatChannel:
				// do nothing
			case <-rf.voteGrantedChannel:
				// do nothing
			case <-rf.winElectionChannel:
				// do nothing
			}
		} else if rf.currentState == Candidate {

			rf.setupCandidateState()
			// parallel-ly broadcast sendRequestVote to all peers
			rf.broadcastSendRequestVoteRPC()

			select {
			case <-rf.heartbeatChannel:
				rf.changeState(Follower)
			case <-rf.winElectionChannel:
				rf.changeState(Leader)
				//panic("unimplemented")
			case <-rf.voteGrantedChannel:
				// do nothing
			case <-time.After(rf.getRandomizedTimoutValue(BaseTime, RandomizedTime)):
				// stays in Candidate state & do nothing
			}
		}
	}
}

// Make
// the service or tester wants to create a Raft server. the ports
// of all the Raft servers (including this one) are in peers[]. this
// server's port is peers[me]. all the servers' peers[] arrays
// have the same order. applyCh is a channel on which the
// tester or service expects Raft to send ApplyMsg messages.
// Make() must return quickly, so it should start goroutines
// for any long-running work.
//
func Make(peers []*labrpc.ClientEnd, me int,
	applyCh chan ApplyMsg) *Raft {

	// Your initialization code here (2A, 2B).
	rf := &Raft{}
	rf.peers = peers
	rf.me = me
	rf.currentTerm = 0
	rf.currentState = Follower
	rf.votedFor = -1
	rf.voteReceived = 0
	rf.logs = append(rf.logs, Log{Term: 0}) // Dummy initial log
	rf.commitIndex = 0
	rf.lastApplied = 0
	rf.nextIndex = make([]int, len(rf.peers))
	rf.matchIndex = make([]int, len(rf.peers))
	rf.winElectionChannel = make(chan bool, ChannelBufferSize)
	rf.voteGrantedChannel = make(chan bool, ChannelBufferSize)
	rf.heartbeatChannel = make(chan bool, ChannelBufferSize)
	rf.applyChannel = applyCh

	go rf.run()

	return rf
}
