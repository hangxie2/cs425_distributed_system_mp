## Environment Setup Guide

- Run the following command in your current terminal:

  - `make build` 

  - This make command will set up the virtual environment in the local directory and install all dependencies required for this project.

- Once we finish build our mp, run `source venv/bin/activate` to enter the virtual environment. 
- Use `python3 directory/to/your/target/file.py` to run the desired python file.



## Reset Environment
- To clean the current virtual environment, run: 
  
  - `make clean`

