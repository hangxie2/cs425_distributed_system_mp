SERVER_IDENTIFIER = 0
SERVER_ADDRESS = 1
SERVER_PORT = 2
ACCEPTING_PEER_PORT = 3

RECONNECT_TIME = 1  # 1s

# Print Colored Log
CRED = '\033[91m'
CGREEN = '\033[92m'
CEND = '\033[0m'
CPINK = '\033[95m'

ENCODING_FORMAT = 'utf-8'
MSG_LENGTH = 2000
