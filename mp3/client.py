import random
from constants import SERVER_IDENTIFIER, SERVER_ADDRESS, SERVER_PORT, ENCODING_FORMAT, MSG_LENGTH
# from utils import gprint, pprint
import os
import sys
import socket

# PORT = 5678
PORT = 8888
CURRNT_SERVER = socket.gethostbyname(socket.gethostname())
ADDR = (CURRNT_SERVER, PORT)
SERVER_ID_TO_IP_MAPPING = {}
IP_TO_SERVER_ID_MAPPING = {}


def parse_config(config_path: str) -> dict:
    servers = {}
    with open(config_path, 'r') as file:
        for line in file:
            server_info = line.split(" ")
            if len(server_info) == 3:
                servers[server_info[SERVER_IDENTIFIER]] = {
                    SERVER_IDENTIFIER: server_info[SERVER_IDENTIFIER],
                    SERVER_ADDRESS: server_info[SERVER_ADDRESS],
                    SERVER_PORT: int(server_info[SERVER_PORT].rstrip())
                }
    return servers


def pick_coordinator(servers: dict):
    return random.choice(list(servers.items()))


def connect(coordinator_info: tuple):
    coordinator_ip_address = SERVER_ID_TO_IP_MAPPING[coordinator_info[SERVER_IDENTIFIER]]
    # coordinator_address = coordinator_info[1][SERVER_ADDRESS]
    coordinator_port_number = coordinator_info[1][SERVER_PORT]
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    address = (coordinator_ip_address, coordinator_port_number)
    # pprint(f'[connect] - client connecting to address {address}')
    client.connect(address)
    # pprint(f'[connect] - client connecting to address {address} success!')
    return client


def bind():
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind(ADDR)
    server.listen()
    conn, addr = server.accept()
    # gprint(f"[Bind] - Bind to {addr} success!")
    return conn


def send_operation(operation_str, socket):
    msg = operation_str.encode(ENCODING_FORMAT)  # todo: upper case
    msg_length = len(msg)
    # send_length = str(msg_length).encode(ENCODING_FORMAT)
    msg += b' ' * (MSG_LENGTH - msg_length)
    # pprint(f'[send_operation] - {locals()}')
    # pprint(f'[send_operation] - msg.length : {len(msg)}')
    socket.sendall(msg)


def map_hostname_to_ip(servers):
    for server_id, server in servers.items():
        SERVER_ID_TO_IP_MAPPING[server_id] = socket.gethostbyname(server[SERVER_ADDRESS]) \
            if server[SERVER_ADDRESS] != 'localhost' else socket.gethostbyname(socket.gethostname())
        IP_TO_SERVER_ID_MAPPING[SERVER_ID_TO_IP_MAPPING[server_id]] = server_id


if __name__ == '__main__':
    client_name = sys.argv[1]
    config_path = sys.argv[2]
    servers = parse_config(config_path)
    map_hostname_to_ip(servers)

    coordinator_info = pick_coordinator(servers)
    client_to_coordinator_socket = connect(coordinator_info)
    # pprint('[client_to_coordinator_socket] CONNECT SUCCESS!')
    # coordinator_to_client_socket = bind()
    # pprint('[bind] BIND SUCCESS!')
    # print(coordinator_info)
    try:
        while True:
            operation = input()
            send_operation(operation, client_to_coordinator_socket)
            reply = client_to_coordinator_socket.recv(MSG_LENGTH).decode(ENCODING_FORMAT).strip()
            # print(reply)  # todo: may need to parse the reply & add multiple condition checker
            if reply == 'True':
                print('OK')
            else:
                print(reply)
            if 'ABORTED' in reply or reply == 'COMMIT OK':
                break
            # todo: add end condition
    except EOFError:
        # gprint("[Client] - All commands in .txt file executed. Client Exits..")
        os._exit(0)
    finally:
        client_to_coordinator_socket.close()
        # gprint("[Client] - client_to_coordinator_socket CLOSED..")
        # coordinator_to_client_socket.close()
