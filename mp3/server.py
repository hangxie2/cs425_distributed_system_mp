import copy
import json
import sys
import time
import traceback
from utils import rprint
# from utils import gprint, rprint, pprint
from constants import SERVER_IDENTIFIER, SERVER_ADDRESS, SERVER_PORT, RECONNECT_TIME, MSG_LENGTH, ENCODING_FORMAT, \
    ACCEPTING_PEER_PORT
import socket
import threading
from classDef import WriteEntry, Account

# raw operation 格式 BALANCE A.foo
SERVER_ID_TO_IP_MAPPING = {}
IP_TO_SERVER_ID_MAPPING = {}
CURRENT_SERVER_ID = ''
CURRENT_SERVER_IP = socket.gethostbyname(socket.gethostname())
SERVERS = {}
CURRENT_SERVER_PORT = -1
BASE_PORT_NUMBER = 12000
# !!! TW 要用于顺延候选的时候要进行排序，才行！！！ 直接用sorted 对key 进行排序就好
# 关于write_commited_timestamp 变成list 的改变，虽然我感觉不用改成list， 因为ta 的例子错了，但是吧改了之后全都加了sort， 效果应该差不多把
accounts_map = dict()


# class SelfNode:
#     def __int__(self, is_coordinator):
#         self.is_coordinator = is_coordinator
#         self.lock = threading.Lock()

class SelfNode:
    def __init__(self):
        self.lock = threading.Lock()


SELF_NODE = SelfNode()


# 逻辑部分
# 确定要commit，执行 才会 去用这个 属于2 phrase commit
# commit 动作本身
def commit_ok_transaction(timestamp: str, self_node: SelfNode):
    if not self_node.lock.locked():
        self_node.lock.acquire()
    try:
        # pprint(f'[commit_ok_transaction] - {timestamp} - ENTER CHECKPOINT')
        # timestamp 是肯定可以commit
        has_account_modified = False
        for cur_account in accounts_map.values():
            # 删除这个accountRTS 中所有的timestamp
            cur_account.RTS = list(filter(lambda x: x != timestamp, cur_account.RTS))
            # TW 如果TW 中没有这个timestamp（transaction) 就不会有任何变动

            if timestamp in cur_account.TW.keys():
                # cur_account.TW.pop(timestamp)
                cur_account.write_timestamp_committed.append(timestamp)
                cur_account.write_timestamp_committed.sort()
                # 更新balance:
                # __import__('pudb').set_trace()
                cur_account.balance += cur_account.TW[timestamp].operation_value
                cur_account.TW.pop(timestamp)
                has_account_modified = True
                # break
        # self_node.lock.release()
        # pprint(f'[commit_ok_transaction] - {timestamp} - EXIT CHECKPOINT')
        if has_account_modified:
            print_all_available_accounts_balance()
    except Exception as e:
        # pass
        rprint(f"[ERROR] commit_ok_transaction error:{e.__class__.__name__}.{e}.{traceback.format_exc()}")
    finally:
        if self_node.lock.locked():
            self_node.lock.release()
    return True


# 收到commit 的消息，要去check
def commit_check_transaction(timestamp: str, self_node: SelfNode) -> bool:
    # pprint(f'[commit_check_transaction] - {timestamp} - ENTER CHECKPOINT')
    if not self_node.lock.locked():
        self_node.lock.acquire()
    try:
        # temp_accounts = copy.deepcopy(accounts_map)
        # 先check tentative write 有没有还没有commit 但是比自己小的
        # check RTS 和 TW
        commit_ok = True
        # self_node.lock.acquire()
        # for cur_account in accounts_map.values():
        for _key, cur_account in accounts_map.items():

            TW = cur_account.TW
            RTS = cur_account.RTS

            # serial
            if timestamp in TW.keys() or timestamp in RTS:  # 如果这个transaction和当前的account 相关的话
                if timestamp in TW.keys():
                    flag = True
                    while flag:
                        # pprint('[commit_check_transaction] - sleeping check 1...')
                        min_write_timestamp = timestamp
                        for temp in TW.keys():
                            if temp < min_write_timestamp:
                                min_write_timestamp = temp
                        if min_write_timestamp != timestamp:
                            self_node.lock.release()  # 放开锁，等一会，再加上锁继续check
                            time.sleep(0.01)
                            self_node.lock.acquire()
                        else:
                            flag = False

                if timestamp in RTS:
                    flag = True
                    while flag:
                        # __import__('pudb').set_trace()
                        # pprint('[commit_check_transaction] - sleeping check 2...')
                        # 这个时候还是有锁的，因为是从flag == false过来的
                        # min_read_timestamp = min(RTS)
                        # min_read_timestamp = min(list(filter(lambda x: x != 0, RTS)))  # DELETE 0 IN THIS CASE
                        # __import__('pudb').set_trace()
                        # pprint(f'[commit_check_transaction] - accounts_map[_key].RTS: {accounts_map[_key].RTS}')
                        min_read_timestamp = min(list(filter(lambda x: x != 0, accounts_map[_key].RTS)))
                        if min_read_timestamp == timestamp:
                            flag = False
                        else:
                            self_node.lock.release()
                            time.sleep(0.01)
                            self_node.lock.acquire()
            else:
                continue  # 如果这个transaction和这个没有关系的话

            # consistency 所有账户的值不能为0 , 取出对应的TW 的write entry 加一加试试看
            # 如果这个transaction 自己没有写过那就不用了
            if timestamp in TW.keys():  # transaction 对这个account 写过了
                tentative_write = TW[timestamp]  # 取出tentative write
                operation_value = tentative_write.operation_value  # 取出要改动多少钱
                if cur_account.balance + operation_value >= 0:  # 查下来大于等于0 可以
                    continue
                else:
                    commit_ok = False  # 如果不大于零不行
            else:
                continue
            # self_node.lock.release()

        # pprint(f'[commit_check_transaction] - {timestamp} - EXIT CHECKPOINT')
        # 如果transaction和这个server中所有的account都没有关系的话, 那么还是返回true
        # 如果说有关系, 那违反consistency, 那就不行
        # 如果不违反, 只是前面的transaction没有commit, 那就是等着
        return commit_ok
    except Exception as e:
        pass
        # rprint(f"[ERROR] commit_check_transaction error:{e.__class__.__name__}.{e}.{traceback.format_exc()}")
    finally:
        if self_node.lock.locked():
            self_node.lock.release()


# return true 表示写成功了
# DEPOSIT && WITHDRAW GET INSIDE
def tentative_write(self_node: SelfNode, timestamp: str, rawOperation: str):
    if not self_node.lock.locked():
        self_node.lock.acquire()

    try:
        # pprint(f'[tentative_write] - {timestamp} - {rawOperation} - ENTER CHECKPOINT')
        # __import__('pudb').set_trace()
        # raw operiation  # deposit 和withdraw还不一样
        account_name = rawOperation.split()[1]  # DEPOSIT A.foo 50
        operation_type = rawOperation.split()[0]
        operation_value = int(rawOperation.split()[2])
        if operation_type == "DEPOSIT":
            # self_node.lock.acquire()
            if account_name in accounts_map:
                # 按照协议来
                cur_account = accounts_map[account_name]
                #  todo: double write
                # __import__('pudb').set_trace()
                if timestamp >= max(cur_account.RTS) and timestamp > sorted(cur_account.write_timestamp_committed)[-1]:
                    if timestamp in cur_account.TW.keys():
                        # 更新TW
                        cur_account.TW[timestamp].operation_value += operation_value
                        # self_node.lock.release()
                        # pprint(f'[tentative_write] - {timestamp} - {rawOperation} - EXIT CHECKPOINT 1')
                        return True
                    else:
                        new_tentative_write = WriteEntry(timestamp, operation_value)
                        cur_account.TW[timestamp] = new_tentative_write
                        # !!!用TW的时候需要对TW 进行排序
                        # self_node.lock.release()
                        # pprint(f'[tentative_write] - {timestamp} - {rawOperation} - EXIT CHECKPOINT 2')
                        return True
                else:
                    self_node.lock.release()
                    # pprint(f'[tentative_write] - {timestamp} - {rawOperation} - EXIT CHECKPOINT 3')
                    return None  # 需要abort
            else:  # 如果deposit 一个没有存在的账户，是可以的
                new_account = Account(account_name, timestamp)
                new_account.TW[timestamp] = WriteEntry(timestamp, operation_value)
                accounts_map[account_name] = new_account  # todo: to be checked with Yan
                # self_node.lock.release()
                # pprint(f'[tentative_write] - {timestamp} - {rawOperation} - EXIT CHECKPOINT 4')
                return True  #

        else:  # == "WITHDRAW" 是read 和write的结合
            # 调用read
            readOperation = "READ" + " " + account_name
            read_result = read(self_node, timestamp, readOperation)

            if read_result is None:
                # pprint(f'[tentative_write] - {timestamp} - {rawOperation} - EXIT CHECKPOINT 4.5')
                return None
            elif read_result == 'NOT FOUND, ABORTED':
                # 需要abort了 并且是 NOT FOUND, ABORTED
                # self_node.lock.release()
                # pprint(f'[tentative_write] - {timestamp} - {rawOperation} - EXIT CHECKPOINT 5')
                return 'NOT FOUND, ABORTED'
            else:
                # 这里默认传入的operation_value 是有正负之分的
                readOperation = "DEPOSIT" + " " + account_name + " " + str(operation_value * -1)
                # pprint(f'[tentative_write] - {timestamp} - {rawOperation} - EXIT CHECKPOINT 6')
                return tentative_write(self_node, timestamp, readOperation)  # todo: one more params?
    except Exception as e:
        pass
        # rprint(f"[ERROR] tentative_write error:{e.__class__.__name__}.{e}.{traceback.format_exc()}")
    finally:
        if self_node.lock.locked():
            self_node.lock.release()
    # self_node.lock.acquire()
    # if account_name in accounts_map:
    #     cur_account: Account = accounts_map[account_name]
    #     max_read_timestamp = cur_account.RTS. ()[-1]
    #
    #     if timestamp >= max_read_timestamp and timestamp> cur_account.write_timestamp_committed:
    #         if timestamp in cur_account.TW.keys():
    #             former_operation_value = cur_account.TW[timestamp].operation_value
    #             now = former_operation_value + operation_value
    #             cur_account.TW[timestamp].operation_value = now
    #             self_node.lock.release()
    #             return 1 #表示写成功了
    #         else:
    #             cur_account.TW[timestamp] = WriteEntry(timestamp,operation_value)
    #             self_node.lock.release()
    #             return 1 # 表示写成功了
    #     else:
    #         self_node.lock.release()
    #         return None # 表示需要abort
    #
    # else: # 如果account 没有是新的 两种情况1，deposit 可以 2. withdraw 不行
    #     operation_name = rawOperation.split()[0]
    #     if operation_name == "DEPOSIT":
    #         account_name = rawOperation.split()[1]
    #         balance = rawOperation.split()[2]
    #         new_account = Account(account_name)
    #         new_account.TW[timestamp] = WriteEntry(timestamp, operation_value)
    #         self_node.lock.release()
    #     else:
    #         self_node.lock.release()
    #         return None


# BALANCE COME HERE
def read(self_node: SelfNode, timestamp: str, rawOperation: str):
    # pprint(f'[read] - {timestamp} - {rawOperation} - ENTER CHECKPOINT')

    if not self_node.lock.locked():
        self_node.lock.acquire()

    try:
        flag = True
        account_name = rawOperation.split()[1]  # BALANCE
        # self_node.lock.acquire()
        if account_name not in accounts_map.keys():
            # self_node.lock.release()
            # pprint(f'[read] - {timestamp} - {rawOperation} - EXIT CHECKPOINT 1')
            return 'NOT FOUND, ABORTED'  # 表示没有读到 需要abort
        while flag:
            # self_node.lock.acquire()
            if account_name not in accounts_map.keys():
                # self_node.lock.release()
                # pprint(f'[read] - {timestamp} - {rawOperation} - EXIT CHECKPOINT 2')
                return 'NOT FOUND, ABORTED'  # 表示没有读到 需要abort

            cur_account: Account = accounts_map[account_name]
            # __import__('pudb').set_trace()
            #  cur_account.write_timestamp_committed 如果是empty的话 也可以进该if
            #  Ref: https://campuswire.com/c/G3023A061/feed/664
            #  todo: to be checked with yan
            if timestamp > sorted(cur_account.write_timestamp_committed)[-1]:
                # Ds = 0
                isCommited = False
                Ds_written_by = -1

                # 这个因为是把write_timestamp_committed 这个改成了list 所以才舍弃下面的，写了下下面的
                # if len(cur_account.TW) != 0:
                #     for ten_timestamp in cur_account.TW.keys():
                #         if ten_timestamp > Ds_written_by:
                #             Ds = cur_account.TW[ten_timestamp].operation_value
                #             Ds_written_by = ten_timestamp
                # # 再和commited 的比较 当TW 为空的时候会落到这里
                # if cur_account.write_timestamp_committed.sort()[-1] > Ds_written_by:
                #     Ds = cur_account.balance
                #     isCommited = True

                # 在TW 和 commited timestamp 中找 <= 自己的最大的
                for ten_timestamp in cur_account.TW.keys():
                    if ten_timestamp <= timestamp and ten_timestamp > Ds_written_by:
                        Ds_written_by = ten_timestamp
                        # Ds = cur_account.TW[ten_timestamp].operation_value

                #  todo: here exists an issue need to check with yan
                if Ds_written_by < sorted(cur_account.write_timestamp_committed)[-1] <= timestamp:
                    # Ds = cur_account.balance
                    Ds_written_by = sorted(cur_account.write_timestamp_committed)[-1]
                    isCommited = True

                if isCommited:
                    flag = False
                    # 传回 应该有的值， 传回coordinator的在其他函数写
                    # ！！！如何确定这个transaction的coordinator是谁啊！！

                    if rawOperation.split()[0] == "READ":  # 是withdraw中的一步，不用在RTS 中写东西
                        cur_balance = cur_account.balance
                        if timestamp in cur_account.TW.keys():
                            new_temp_balance_this_timestamp = cur_balance + cur_account.TW[timestamp].operation_value
                            # self_node.lock.release()
                            # pprint(f'[read] - {timestamp} - {rawOperation} - EXIT CHECKPOINT 4.4.1')
                            return new_temp_balance_this_timestamp
                        else:
                            # pprint(f'[read] - {timestamp} - {rawOperation} - EXIT CHECKPOINT 4.4.2')
                            return cur_balance

                    cur_account.RTS.append(timestamp)
                    cur_balance = cur_account.balance
                    if timestamp in cur_account.TW.keys():
                        new_temp_balance_this_timestamp = cur_balance + cur_account.TW[timestamp].operation_value
                        # self_node.lock.release()
                        # pprint(f'[read] - {timestamp} - {rawOperation} - EXIT CHECKPOINT 4.5.1')
                        return new_temp_balance_this_timestamp
                    else:
                        # pprint(f'[read] - {timestamp} - {rawOperation} - EXIT CHECKPOINT 4.5.2')
                        return cur_balance
                else:
                    if Ds_written_by == timestamp:
                        cur_balance = cur_account.balance
                        if timestamp in cur_account.TW.keys():
                            new_temp_balance_this_timestamp = cur_balance + cur_account.TW[timestamp].operation_value
                            flag = False
                            # self_node.lock.release()
                            # pprint(f'[read] - {timestamp} - {rawOperation} - EXIT CHECKPOINT 5.1')
                            return new_temp_balance_this_timestamp
                            # 传回 应该有的值， 传回coordinator的在其他函数写
                        else:
                            # pprint(f'[read] - {timestamp} - {rawOperation} - EXIT CHECKPOINT 5.2')
                            return cur_balance
                    else:
                        # pprint(f'[read] - {timestamp} - {rawOperation} - SLEEPING...')
                        self_node.lock.release()
                        time.sleep(0.01)
                        self_node.lock.acquire()
            else:
                # abort(没写), 传输给coordinator 没写
                # self_node.lock.release()
                # pprint(f'[read] - {timestamp} - {rawOperation} - EXIT CHECKPOINT 6')
                return None
    except Exception as e:
        pass
        # rprint(f"[ERROR] read error:{e.__class__.__name__}.{e}.{traceback.format_exc()}")
    finally:
        if self_node.lock.locked():
            self_node.lock.release()


# abort 的本身
def abort(self_node: SelfNode, timestamp: str):
    # pprint(f'[abort] - {timestamp} - ENTER CHECKPOINT')

    # #对当前server 所有的account 的TW 和RTS 中进行查找看看这个timestamp 的东西
    # self_node.lock.acquire()
    # for cur_account in accounts_map.values():
    #     #对TW 遍历 要在本身TW 上做修改
    #     for timestamp_in_TW in cur_account.TW.keys():
    #         if timestamp_in_TW == timestamp:
    #             cur_account.TW.pop(timestamp)
    #     #对RTS中的元素进行删除
    #     cur_account.RTS = list(filter(lambda x: x != timestamp, cur_account.RTS))
    # self_node.lock.release()

    # 遍历accountmap， 看看account是否和当前timestamp有关
    # 如果说 account created_by 字段 == timestamp一样的————> 如果说在TW中没有可以顺延的，那就删掉account；如果有TW 有可顺延的，更新createdby 并且删除掉 在TW和RTS中相关的
    self_node.lock.acquire()
    try:
        for cur_account_name in list(accounts_map.keys()):
            cur_account = accounts_map[cur_account_name]
            if timestamp == cur_account.created_by:
                if len(cur_account.TW) == 1:  # TW 中只有这个timestamp
                    del accounts_map[cur_account_name]
                else:
                    # 将TW 中的自己删掉，进行顺延一个赋值给created_by
                    del cur_account.TW[timestamp]
                    # 取出顺延的transaction 的timestamp 需要对TW 进行排序
                    # new_created_by = list(cur_account.TW.keys())[0]
                    new_created_by = sorted(cur_account.TW.keys())[0]
                    cur_account.created_by = new_created_by
                    # 将RTS  中的自己也删掉
                    cur_account.RTS = list(filter(lambda x: x != timestamp, cur_account.RTS))
            else:  # created_by 字段和自己不一样, 直接在删除就可，如果说存在的话
                # 先删除RTS
                cur_account.RTS = list(filter(lambda x: x != timestamp, cur_account.RTS))
                # 再删除 TW
                # 判断TW 中是不是有这个timestamp
                if timestamp in list(cur_account.TW.keys()):
                    del cur_account.TW[timestamp]
            # self_node.lock.release()
        # pprint(f'[abort] - {timestamp} - EXIT CHECKPOINT')
    except Exception as e:
        pass
        # rprint(f"[ERROR] abort error:{e.__class__.__name__}.{e}.{traceback.format_exc()}")
    finally:
        self_node.lock.release()


# 链接部分
def parse_config(config_path: str) -> dict:
    servers = {}
    designated_port_number = BASE_PORT_NUMBER
    with open(config_path, 'r') as file:
        for line in file:
            server_info = line.split(" ")
            if len(server_info) == 3:
                servers[server_info[SERVER_IDENTIFIER]] = {
                    SERVER_IDENTIFIER: server_info[SERVER_IDENTIFIER],
                    SERVER_ADDRESS: server_info[SERVER_ADDRESS],
                    SERVER_PORT: int(server_info[SERVER_PORT].rstrip()),
                    ACCEPTING_PEER_PORT: designated_port_number
                }
            designated_port_number += 100
    return servers


def map_hostname_to_ip(servers):
    for server_id, server in servers.items():
        SERVER_ID_TO_IP_MAPPING[server_id] = socket.gethostbyname(server[SERVER_ADDRESS]) \
            if server[SERVER_ADDRESS] != 'localhost' else socket.gethostbyname(socket.gethostname())
        IP_TO_SERVER_ID_MAPPING[SERVER_ID_TO_IP_MAPPING[server_id]] = server_id


def establish_bidirectional_connection(servers, current_server_id):
    return_values = {}

    # thread1 = threading.Thread(target=bind_with_all_servers,
    #                            args=[copy.deepcopy(servers), current_server_id, return_values])
    thread2 = threading.Thread(target=connect_with_all_servers,
                               args=[copy.deepcopy(servers), current_server_id, return_values])

    # thread1.start()
    thread2.start()
    # thread1.join()
    thread2.join()

    return return_values


def connect_with_all_servers(servers: dict, current_server_id: str, return_values: dict):
    servers.pop(current_server_id, None)
    connect_info = {}
    # connect with other nodes
    while servers:
        remaining_nodes = {}
        for server_id, server in servers.items():
            try:
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                host_ip_address = SERVER_ID_TO_IP_MAPPING[server[SERVER_IDENTIFIER]]
                # server[SERVER_PORT] = SERVERS[CURRENT_SERVER_ID][SERVER_PORT]+1  # todo: to be deleted
                address = (host_ip_address, server[ACCEPTING_PEER_PORT])
                # pprint(f"[connect_with_all_servers] - {CURRENT_SERVER_ID} is trying to "
                #        f"connect {(host_ip_address, server[ACCEPTING_PEER_PORT])}")
                s.connect(address)
                connect_info[server_id] = s
                # gprint(f"[Connect] - "
                #        f"Connecting to {server[SERVER_IDENTIFIER]}: {host_ip_address}:"
                #        f"{server[ACCEPTING_PEER_PORT]} success!")
            except Exception:
                # rprint(f"[Connect] - Connection to "
                #        f"{server[SERVER_IDENTIFIER]} failed. "
                #        f"Reconnecting in {RECONNECT_TIME}s..")
                remaining_nodes[server_id] = server
        servers = remaining_nodes
        time.sleep(RECONNECT_TIME)
    return_values['connect_info'] = connect_info
    # gprint("[Connect] - Connect to all servers success!")


# def bind_with_all_servers(servers: dict, current_server_id: str, return_values: dict):
#     # port = servers.get(current_server_id, {}).get(SERVER_PORT, 1234)
#     global PORT
#     port = PORT
#     PORT += 1
#     s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#     gprint(f"[Bind] - Current attempt bind server {(SERVER, port)}")
#     try:
#         s.bind((SERVER, port))
#     except Exception as e:
#         rprint(f"[Bind Error] - Current address already in use: {(SERVER, port)}. "
#                f"Please release this address before running this app : )")
#         rprint(f"[ERROR] receiving_handler error:{e.__class__.__name__}.{e}.{traceback.format_exc()}")
#         os._exit(0)
#
#     s.listen()
#     gprint(f"[Bind] - Server is listening on {(SERVER, port)}")
#
#     bind_info = {}
#     servers.pop(current_server_id, None)
#     while True:
#         conn, addr = s.accept()
#         gprint(f"[Bind] - Bind to {addr} success!")
#         bind_info[IP_TO_SERVER_ID_MAPPING[addr[0]]] = conn
#         if len(bind_info) == len(servers):
#             break
#
#     return_values['bind_info'] = bind_info
#     gprint("[Bind] - Bind to all servers success!")


def extract_target_server(operation):
    try:
        return operation.split(' ')[1].split('.')[0]
    except Exception as e:
        pass
        # rprint(f"[ERROR] extract_target_server error:{e.__class__.__name__}.{e}.{traceback.format_exc()}")


def send_msg_to_socket(msg, socket):
    msg = msg.encode(ENCODING_FORMAT)
    msg_length = len(msg)
    # send_length = str(msg_length).encode(ENCODING_FORMAT)
    msg += b' ' * (MSG_LENGTH - msg_length)
    # pprint(f'[send_msg_to_socket] - msg.length : [{len(msg)}], msg: [{msg.strip()}]')

    socket.sendall(msg)


def operation_handler(operation, timestamp):
    ret_value = None
    if operation.startswith('DEPOSIT') or operation.startswith('WITHDRAW'):
        ret_value = tentative_write(self_node=SELF_NODE, timestamp=timestamp, rawOperation=operation)
    elif operation.startswith('BALANCE'):
        ret_value = read(self_node=SELF_NODE, timestamp=timestamp, rawOperation=operation)
    return ret_value


def broadcast_msg(operation, connect_info):
    res = []
    for node_id, each_socket in connect_info.items():
        # gprint(f'[{CURRENT_SERVER_ID}] - Multicasting [{operation}] to [{node_id}] NOW!')
        send_msg_to_socket(operation, each_socket)
        # gprint(f'[{CURRENT_SERVER_ID}] - Multicasting [{operation}] to [{node_id}] DONE!')

    if 'PREPARE COMMIT' in operation:
        for node_id, each_socket in connect_info.items():
            # gprint(f'[{CURRENT_SERVER_ID}] - Multicasting [{operation}] to [{node_id}] NOW!')
            # send_msg_to_socket(operation, each_socket)
            prepare_res = each_socket.recv(MSG_LENGTH).decode(ENCODING_FORMAT).strip()
            # gprint(f'[{CURRENT_SERVER_ID} PREPARE COMMIT] - RECEIVING from [{node_id}]: {prepare_res}')
            res.append(eval(prepare_res))
            # gprint(f'[{CURRENT_SERVER_ID}] - Multicasting [{operation}] to [{node_id}] DONE!')

    # pprint(f'[broadcast_msg] - res: {res}')
    return res


def format_operation_and_timestamp_json(operation, timestamp):
    formatted_json = {
        'operation': operation,
        'timestamp': timestamp
    }
    return str(json.dumps(formatted_json))


def client_handler(conn, SELF_NODE):
    # pprint('[client_handler] CHECKPOINT -2')
    return_values = establish_bidirectional_connection(SERVERS, CURRENT_SERVER_ID)
    # pprint('[client_handler] CHECKPOINT -1')
    connect_info = return_values.get('connect_info', {})
    # bind_info = return_values.get('bind_info', {})
    timestamp = time.time()
    # pprint('[client_handler] CHECKPOINT 0')
    # pprint(f'[client_handler] - current customer timestamp {timestamp}')

    try:
        while True:
            # pprint('---------------------------------------------------------------')
            # pprint('[client_handler] CHECKPOINT 1')
            operation = conn.recv(MSG_LENGTH).decode(ENCODING_FORMAT).strip()
            # pprint('[client_handler] CHECKPOINT 2')
            operation_json = format_operation_and_timestamp_json(operation, timestamp)
            # gprint(f'[client_handler] - operation [{operation}] received success!')

            if not operation:
                break
            elif operation.startswith('BEGIN'):
                send_msg_to_socket("OK", conn)
            else:
                target_server_id = extract_target_server(operation)
                target_server_connect_socket = connect_info.get(target_server_id)
                # target_server_bind_socket = bind_info.get(target_server_id)
                is_self = target_server_id == CURRENT_SERVER_ID
                # gprint(f'[client_handler] - operation [{operation}] SENDING TO {target_server_id}')
                ret_value = None
                if operation.startswith('DEPOSIT') or operation.startswith('WITHDRAW'):
                    if is_self:
                        ret_value = tentative_write(self_node=SELF_NODE, timestamp=timestamp, rawOperation=operation)
                        # gprint(
                        #     f'[server_handler] accounts_map: '
                        #     f'\n{json.dumps(accounts_map, default=lambda x: x.__dict__)}')
                    else:
                        send_msg_to_socket(operation_json, target_server_connect_socket)
                        # ret_value = target_server_bind_socket.recv(MSG_LENGTH).decode(ENCODING_FORMAT)
                        ret_value = target_server_connect_socket.recv(MSG_LENGTH).decode(ENCODING_FORMAT)
                elif operation.startswith('BALANCE'):
                    if is_self:
                        ret_value = read(self_node=SELF_NODE, timestamp=timestamp, rawOperation=operation)
                        # gprint(
                        #     f'[server_handler] accounts_map: '
                        #     f'\n{json.dumps(accounts_map, default=lambda x: x.__dict__)}')
                    else:
                        send_msg_to_socket(operation_json, target_server_connect_socket)
                        # ret_value = target_server_bind_socket.recv(MSG_LENGTH).decode(ENCODING_FORMAT)
                        ret_value = target_server_connect_socket.recv(MSG_LENGTH).decode(ENCODING_FORMAT)
                elif operation.startswith('COMMIT'):
                    prepare_msg = format_operation_and_timestamp_json('PREPARE COMMIT', timestamp)
                    prepare_res = broadcast_msg(prepare_msg, connect_info)
                    self_prepare_res = commit_check_transaction(timestamp, SELF_NODE)
                    prepare_res.append(self_prepare_res)
                    if not all(prepare_res):
                        # 2-phase commit first phase failed
                        # broadcast_msg(format_operation_and_timestamp_json('ABORT', timestamp), connect_info)
                        # abort(SELF_NODE, timestamp)
                        ret_value = 'ABORTED'  # todo
                    else:
                        # 2-phase commit first phase success
                        broadcast_msg(operation_json, connect_info)
                        commit_ok_transaction(timestamp, SELF_NODE)
                        ret_value = 'COMMIT OK'  # todo
                    # gprint(f'[server_handler] accounts_map: \n'
                    #        f'{json.dumps(accounts_map, default=lambda x: x.__dict__)}')

                elif operation.startswith('ABORT'):
                    # __import__('pudb').set_trace()
                    # broadcast_msg(operation_json, connect_info)
                    # abort(SELF_NODE, timestamp)
                    ret_value = 'ABORTED'  # todo
                    # gprint(f'[server_handler] accounts_map: '
                    #        f'\n{json.dumps(accounts_map, default=lambda x: x.__dict__)}')

                ret_value = reformat_return_value(ret_value, operation)
                if 'ABORT' in ret_value:
                    broadcast_msg(format_operation_and_timestamp_json('ABORT', timestamp), connect_info)
                    abort(SELF_NODE, timestamp)

                send_msg_to_socket(ret_value, conn)
    except Exception as e:
        pass
        # rprint(f"[ERROR] client_handler error:{e.__class__.__name__}.{e}.{traceback.format_exc()}")
    finally:
        # pprint("[client_handler] CONNECTION CLOSED...")
        conn.close()
        # client_recv_socket.close()
        # client_send_socket.close()


def reformat_return_value(ret_value, operation):
    if isinstance(ret_value, bool):
        if ret_value:
            return 'True'
        else:
            return 'False'
    elif isinstance(ret_value, str):
        return ret_value
    elif isinstance(ret_value, int):
        account_name = operation.split()[1]
        ret_value = f'{account_name} = {ret_value}'
        return str(ret_value)
    elif ret_value == 'NOT FOUND, ABORTED':
        return ret_value
    elif ret_value is None:
        return 'ABORTED'
    else:
        raise TypeError(f'ret_value should be None or a bool, not {ret_value.__class__.__name__}')


def print_all_available_accounts_balance():
    balances_str = ', '.join('{}:{}'.format(k, v.balance) if v.balance != 0
                             else '' for k, v in sorted(accounts_map.items()))
    if balances_str:
        print('BALANCES', end=' ')
        print(balances_str)


def server_handler(conn, SELF_NODE):
    try:
        while True:
            # pprint('---------------------------------------------------------------')
            operation_dict = json.loads(conn.recv(MSG_LENGTH).decode(ENCODING_FORMAT).strip())
            operation = operation_dict.get('operation', '')
            timestamp = operation_dict.get('timestamp', '')
            ret_value = None
            if operation.startswith('DEPOSIT') or operation.startswith('WITHDRAW'):
                ret_value = tentative_write(self_node=SELF_NODE, timestamp=timestamp, rawOperation=operation)
            elif operation.startswith('BALANCE'):
                ret_value = read(self_node=SELF_NODE, timestamp=timestamp, rawOperation=operation)
            elif operation.startswith('PREPARE COMMIT'):
                ret_value = commit_check_transaction(timestamp, SELF_NODE)
            elif operation.startswith('COMMIT'):
                ret_value = commit_ok_transaction(timestamp, SELF_NODE)
                # print_all_available_accounts_balance()
            elif operation.startswith('ABORT'):
                abort(SELF_NODE, timestamp)
                # gprint(f'[server_handler] accounts_map: \n{json.dumps(accounts_map, default=lambda x: x.__dict__)}')
                break
            ret_value = reformat_return_value(ret_value, operation)
            send_msg_to_socket(ret_value, conn)
            # gprint(f'[server_handler] accounts_map: \n{json.dumps(accounts_map, default=lambda x: x.__dict__)}')
    except Exception as e:
        pass
        # rprint(f"[ERROR] server_handler error:{e.__class__.__name__}.{e}.{traceback.format_exc()}")
    finally:
        # pprint('[server_handler] - ABORTED - connection with the current client CLOSED!')
        conn.close()
        # peer_server_send_socket.close()


def accepting_clients(SELF_NODE):
    try:
        server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # pprint(f'[accepting_clients] - {SERVERS}')
        server.bind((CURRENT_SERVER_IP, SERVERS[CURRENT_SERVER_ID][SERVER_PORT]))
        server.listen()
        # print(f"[LISTENING] Server is listening on {ADDR}")  # DEBUG
        while True:
            conn, addr = server.accept()
            # gprint(f'[accepting_clients] - client [{addr}] connect to coordinator [{CURRENT_SERVER_ID}] success!')
            # client_send_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            # client_send_socket.connect((addr[0], 5678))
            # gprint(f'[accepting_clients] - coordinator [{CURRENT_SERVER_ID}] connect to client [({addr[0]}, {5678})] success!')
            # thread = threading.Thread(target=client_handler, args=[conn, SELF_NODE, client_send_socket])
            thread = threading.Thread(target=client_handler, args=[conn, SELF_NODE])
            thread.start()
    except Exception as e:
        pass
        # rprint(f"[ERROR] accepting_clients error:{e.__class__.__name__}.{e}.{traceback.format_exc()}")
    finally:
        server.close()


def accepting_peer_servers(SELF_NODE):
    try:
        server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server.bind((CURRENT_SERVER_IP, SERVERS[CURRENT_SERVER_ID][ACCEPTING_PEER_PORT]))
        # pprint(f'[accepting_peer_servers] - {CURRENT_SERVER_ID} current '
        #        f'bind at {(CURRENT_SERVER_IP, SERVERS[CURRENT_SERVER_ID][ACCEPTING_PEER_PORT])}')
        server.listen()
        # print(f"[LISTENING] Server is listening on {ADDR}")  # DEBUG
        while True:
            conn, addr = server.accept()
            # gprint(f'[accepting_peer_servers] - peer server [{addr}] connect to server [{CURRENT_SERVER_ID}] success!')
            # client_send_socket = server.connect(addr)
            # gprint(f'[accepting_peer_servers] - server [{CURRENT_SERVER_ID}] connect to peer server [{addr}] success!')
            thread = threading.Thread(target=server_handler, args=[conn, SELF_NODE])
            thread.start()
    except Exception as e:
        pass
        # rprint(f"[ERROR] accepting_peer_servers error:{e.__class__.__name__}.{e}.{traceback.format_exc()}")
    finally:
        server.close()


# def accepting_connections(SELF_NODE):
#     try:
#         server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#         server.bind((CURRENT_SERVER_IP, CURRENT_SERVER_PORT))
#         server.listen()
#         # print(f"[LISTENING] Server is listening on {ADDR}")  # DEBUG
#         while True:
#             conn, addr = server.accept()
#             if addr[0] in SERVERS.keys():
#                 thread = threading.Thread(target=server_handler, args=[conn, SELF_NODE])
#                 gprint(
#                     f'[accepting_peer_servers] - peer server [{addr}] connect to server [{CURRENT_SERVER_ID}] success!')
#                 thread.start()
#             else:
#                 thread = threading.Thread(target=client_handler, args=[conn, SELF_NODE])
#                 gprint(f'[accepting_clients] - client [{addr}] connect to coordinator [{CURRENT_SERVER_ID}] success!')
#                 thread.start()
#             # client_send_socket = server.connect(addr)
#             # gprint(f'[accepting_peer_servers] - server [{CURRENT_SERVER_ID}] connect to peer server [{addr}] success!')
#             # thread = threading.Thread(target=server_handler, args=[conn, SELF_NODE])
#             # thread.start()
#     except Exception as e:
#         rprint(f"[ERROR] accepting_peer_servers error:{e.__class__.__name__}.{e}.{traceback.format_exc()}")
#     finally:
#         server.close()


if __name__ == '__main__':
    CURRENT_SERVER_ID = sys.argv[1]
    config_path = sys.argv[2]
    SERVERS = parse_config(config_path)
    map_hostname_to_ip(SERVERS)
    CURRENT_SERVER_PORT = SERVERS[CURRENT_SERVER_ID][SERVER_PORT]

    # pprint(f'[main] - SELF_NODE: {SELF_NODE}')
    # pprint(f'[main] - SELF_NODE.lock: {SELF_NODE.lock}')

    thread1 = threading.Thread(target=accepting_clients, args=[SELF_NODE])
    thread2 = threading.Thread(target=accepting_peer_servers, args=[SELF_NODE])

    thread1.start()
    thread2.start()
    thread1.join()
    thread2.join()

    # thread = threading.Thread(target=accepting_connections, args=[SELF_NODE])
    # thread.start()
    # thread.join()
