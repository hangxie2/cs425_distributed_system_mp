from constants import CRED, CEND, CGREEN, CPINK


def rprint(msg):
    print(CRED + msg + CEND)


def gprint(msg):
    print(CGREEN + msg + CEND)


def pprint(msg):
    print(CPINK + msg + CEND)


def compare_timestamp(timestamp1:str, timestamp2 :str):
    timestamp1_info = timestamp1.split(".")
    timestamp2_info = timestamp2.split(".")
    timestamp1_timestamp = timestamp1_info[0]
    timestamp2_timestamp = timestamp2_info[0]
    timestamp1_client_num = timestamp1_info[1]
    timestamp2_client_num = timestamp2_info[1]

    if timestamp1_timestamp != timestamp1_timestamp:
        return int(timestamp1_timestamp) > int(timestamp2_timestamp)
    else:
        return timestamp1_client_num > timestamp1_client_num
