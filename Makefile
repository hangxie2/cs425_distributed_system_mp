build:
	python3 -m venv venv
	./venv/bin/pip3 install --upgrade pip
	./venv/bin/pip3 install -r requirements.txt
	./venv/bin/pre-commit install
	chmod +x ./mp0/logger
	chmod +x ./mp0/node
	chmod +x ./mp3/server
	chmod +x ./mp3/client

clean:
	rm -rf __pycache__
	rm -rf venv