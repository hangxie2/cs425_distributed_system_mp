import socket
import threading
import json
import traceback
import sys
import logging
import time
import os
from graph_generator import graph_bandwidth, graph_delays
from constants import INITIAL_MSG_LENGTH, REQUEST_DISCONNECT_MSG, ENCODING_FORMAT

logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s | %(levelname)s | %(message)s')
file_handler = logging.FileHandler('logs.log', mode='w')
file_handler.setLevel(logging.INFO)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

timestamp_and_receive_amount_list = []

PORT = ''
SERVER = socket.gethostbyname(socket.gethostname())


def recv_from_client(conn):
    try:
        connected = True
        while connected:
            initial_msg_length = conn.recv(INITIAL_MSG_LENGTH).decode(ENCODING_FORMAT)
            if initial_msg_length:
                initial_msg_length = int(initial_msg_length)
                msg_length = initial_msg_length
                msg = conn.recv(msg_length).decode(ENCODING_FORMAT)
                msg_dict = json.loads(msg)
                timestamp = msg_dict.get('timestamp', '')
                name = msg_dict.get('node_name', '')
                event = msg_dict.get('event', '')
                is_first_msg = msg_dict.get('is_first_msg', '')

                if event == REQUEST_DISCONNECT_MSG:
                    print(f"{timestamp} - {name} disconnected")
                    logger.info(f"{timestamp} - {name} disconnected")
                    break

                if is_first_msg:
                    print(f"{timestamp} - {name} connected")
                    logger.info(f"{timestamp} - {name} connected")
                else:
                    print(f"{timestamp} {name} {event}")
                    timestamp_and_receive_amount_list.append([time.time(), timestamp, msg_length + initial_msg_length])
                    logger.info(f"{timestamp} {str(time.time())} {name} {event} {str(initial_msg_length + msg_length)}")
        conn.close()
    except Exception as e:
        print(f"[ERROR] thread error:{e.__class__.__name__}.{e}.{traceback.format_exc()}")


def start():
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind(ADDR)
    server.listen()
    # print(f"[LISTENING] Server is listening on {ADDR}")  # DEBUG
    while True:
        conn, addr = server.accept()
        thread = threading.Thread(target=recv_from_client, args=[conn])
        thread.start()


if __name__ == '__main__':
    try:
        PORT = int(sys.argv[1])
        ADDR = (SERVER, PORT)
        start()
    except KeyboardInterrupt:
        print('Generating graphs...')
        list_file = open("list_file.txt", "w")
        list_file.write(str(timestamp_and_receive_amount_list))
        list_file.close()
        graph_bandwidth(timestamp_and_receive_amount_list)
        graph_delays(timestamp_and_receive_amount_list)
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
    except Exception as e:
        print(f"[ERROR] port info parsing error:{e.__class__.__name__}.{e}.{traceback.format_exc()}")
