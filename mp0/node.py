import socket
import time
import traceback
import sys
import json
import os
from constants import INITIAL_MSG_LENGTH, REQUEST_DISCONNECT_MSG, ENCODING_FORMAT

LOGGER_SERVER = ''
LOGGER_PORT = ''
NODE_NAME = ''


def start_node():
    # print(f'[START]: {NODE_NAME} ')  # DEBUG
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    address = (LOGGER_SERVER, LOGGER_PORT)
    client.connect(address)
    try:
        event = input()
        send_event(event, client, is_first_msg=True)
        while True:
            event = input()
            send_event(event, client)
            print(event)
    except EOFError:
        send_event(REQUEST_DISCONNECT_MSG, client)
    except Exception as e:
        print(f"[ERROR] start_node error:{e.__class__.__name__}.{e}.{traceback.format_exc()}")
    finally:
        client.close()


def parse_event_to_json_str(event, is_first_msg):
    if event == REQUEST_DISCONNECT_MSG:
        event_json = {
            'event': REQUEST_DISCONNECT_MSG,
            'node_name': NODE_NAME,
            'timestamp': time.time()
        }
    else:
        event_list = event.split(' ')
        event_json = {
            'timestamp': event_list[0],
            'event': event_list[1],
            'node_name': NODE_NAME,
            'is_first_msg': is_first_msg
        }
    return str(json.dumps(event_json))


def send_event(event, client, is_first_msg=False):
    """
    The is_first_msg will be set True when the connection is just established.
    It will send the name and current timestamp of the current node to the logger.
    """
    event_json_str = parse_event_to_json_str(event, is_first_msg)
    msg = event_json_str.encode(ENCODING_FORMAT)
    msg_length = len(msg)
    send_length = str(msg_length).encode(ENCODING_FORMAT)
    send_length += b' ' * (INITIAL_MSG_LENGTH - len(send_length))
    client.send(send_length)
    client.send(msg)


if __name__ == '__main__':
    try:
        NODE_NAME = sys.argv[1]
        LOGGER_SERVER = sys.argv[2]
        LOGGER_PORT = int(sys.argv[3])
        start_node()
    except KeyboardInterrupt:
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
    except Exception as e:
        print(f"[Error] logger info parsing error:{e.__class__.__name__}.{e}.{traceback.format_exc()}")
