import matplotlib.pyplot as plt
import numpy as np


def byte_to_kb(num):
    return (num * 8) / 1024


def graph_bandwidth(timestamp_and_receive_amount_list):
    if not timestamp_and_receive_amount_list:
        print('graph_bandwidth(): Looks like there is no event coming in... Did you start the logger and node? ')
        return

    bandwidth = []
    pre = timestamp_and_receive_amount_list[0][0]
    bandwidth.append(timestamp_and_receive_amount_list[0][2])
    count = 0

    for j in range(1, len(timestamp_and_receive_amount_list)):
        time_now = timestamp_and_receive_amount_list[j][0]
        msg_length = timestamp_and_receive_amount_list[j][2]
        if (time_now - pre) < 1:
            bandwidth[-1] += msg_length
        else:
            gap = int(time_now - pre)
            pre = pre + gap
            if gap > 1:
                for i in range(gap - 1):
                    bandwidth.append(0)
                    count += 1
                count += 1
                bandwidth.append(msg_length)
            else:
                count += 1
                bandwidth.append(msg_length)

    moment = []
    for i in range(len(bandwidth)):
        moment.append(i)

    bandwidth = [byte_to_kb(num) for num in bandwidth]
    plt.figure()
    plt.title("Bandwidth(Kbps)")
    # plt.plot(moment, bandwidth, color='b', marker='o', markerfacecolor='blue', markersize=2)
    plt.plot(moment, bandwidth, color='b')
    plt.xlabel('Second(s)')
    plt.ylabel('Kbps')
    # for a, b in zip(moment, bandwidth):
    #     plt.text(a, b, b, ha='center', va='bottom', fontsize=8)

    plt.legend()
    plt.savefig('bandwidth_graph.png', bbox_inches='tight')
    print('Bandwidth graph generated! ')
    # plt.show()


def graph_delays(timestamp_and_receive_amount_list):
    if not timestamp_and_receive_amount_list:
        print('graph_delays(): Looks like there is no event coming in... Did you start the logger and node? ')
        return

    moment = []
    pre = timestamp_and_receive_amount_list[0][0]
    delay = [[] for i in range(200)]
    minimum = []
    maximum = []
    ninetieth_per = []
    median = []

    count = 0

    for j in range(len(timestamp_and_receive_amount_list)):
        time_now = timestamp_and_receive_amount_list[j][0]
        delay_now = -float(timestamp_and_receive_amount_list[j][1]) + time_now

        if (time_now - pre) < 1:
            delay[count].append(delay_now)
        else:
            gap_d = int(time_now - pre)
            pre = pre + gap_d
            if gap_d > 1:
                for i in range(gap_d - 1):
                    count += 1
                    delay[count].append(0)
                count += 1
                delay[count].append(delay_now)
            else:
                count += 1
                delay[count].append(delay_now)

    for m in range(count + 1):
        delay[m].sort()
        minimum.append(delay[m][0])
        maximum.append(delay[m][-1])
        median.append(np.median(delay[m]))
        pos_90 = int(0.9 * len(delay[m]))
        ninetieth_per.append(delay[m][pos_90])

    for k in range(count + 1):
        moment.append(k)

    plt.figure()
    # plt.title("minimum delay, maximum delay, median delay, 90th percentile delay per second")
    # plt.plot(moment, minimum, label='minimum', marker='o', markerfacecolor='blue', markersize=2)
    # plt.plot(moment, maximum, label='maximum', marker='s', markerfacecolor='red', markersize=2)
    # plt.plot(moment, median, label='median', marker='^', markerfacecolor='green', markersize=2)
    # plt.plot(moment, ninetieth_per, label='90th percentile', marker='*', markerfacecolor='brown', markersize=2)
    plt.title("Minimum Delay per Second")
    # plt.plot("minimum delay per second")
    plt.xlabel('Second(s)')
    plt.ylabel('Delay(s)')
    # print(minimum)
    # print(np.arange(min(minimum), max(minimum), 0.001))
    # plt.yticks(np.arange(min(minimum), max(minimum), 0.001))
    plt.plot(moment, minimum, label='minimum')
    plt.legend(loc='best')
    plt.savefig('minimum_delay_graph.png', bbox_inches='tight')
    # plt.show()

    plt.figure()

    plt.title("Maximum Delay per Second")
    # plt.plot("maximum delay per second")
    plt.xlabel('Second(s)')
    plt.ylabel('Delay(s)')
    plt.plot(moment, maximum, label='maximum')
    plt.legend(loc='best')
    plt.savefig('maximum_delay_graph.png', bbox_inches='tight')
    # plt.show()

    plt.figure()
    plt.title("Median Delay per Second")
    plt.xlabel('Second(s)')
    plt.ylabel('Delay(s)')
    # plt.plot("median delay per second")
    plt.plot(moment, median, label='median')
    plt.legend(loc='best')
    plt.savefig('median_delay_graph.png', bbox_inches='tight')
    # plt.show()

    plt.figure()
    plt.title("90th Percentile Delay per Second")
    plt.xlabel('Second(s)')
    plt.ylabel('Delay(s)')
    # plt.plot("90th percentile delay per second")
    plt.plot(moment, ninetieth_per, label='90th percentile')
    plt.legend(loc='best')
    plt.savefig('90th_percentile_delay_graph.png', bbox_inches='tight')
    print('Delay Graphs generated! ')
    # plt.show()
