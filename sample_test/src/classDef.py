class WriteEntry:
    def __init__(self, timestamp, operation_value):
        self.timestamp = timestamp
        # self.raw_operation = raw_operation
        self.operation_value = operation_value
        self.is_commited = False

        # for comparation
        def __lt__(self, other):
            self_timestamp_info = self.timestamp.split("|")
            other_timestamp_info = other.timestamp.split("|")
            self_timestamp = self_timestamp_info[0]
            other_timestamp = other_timestamp_info[0]
            self_client_num = self_timestamp_info[1]
            other_client_num = other_timestamp_info[1]

            if self_timestamp != other_timestamp:
                return int(self_timestamp) < int(other_timestamp)
            else:
                return self_client_num < other_client_num


class Account:
    def __init__(self, account_name, timestamp):
        self.balance = 0
        self.TW = dict()  # {timestamp: writeentry} #py 3.6 之后自动就会按照顺序插入了
        self.RTS = [0]  # format[timestamp1, timestamp2, timestamp3]
        self.write_timestamp_committed = [0]  # 是一个order 的
        self.created_by = timestamp
