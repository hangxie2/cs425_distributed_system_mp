==========================
=== UNSOLVED CONFLICTS ===
==========================


BEGIN
DEPOSIT D.t 10
DEPOSIT D.t 20 <<<<< D is the coordinator


=====================


BEGIN
DEPOSIT E.E 10
DEPOSIT E.E 20
WITHDRAW E.E 5
WITHDRAW E.E 1
WITHDRAW E.E 24
WITHDRAW E.E 1
BALANCE E.E
-1
COMMIT


=====================

DEPOSIT C.C 300
COMMIT

<<< OPEN A NEW CLIENT

BALANCE C.C
DEPOSIT A.A 50
WITHDRAW A.A 60
DEPOSIT C.E 30
DEPOSIT E.E 50
COMMIT
ABORTED


==========================
===== SOLVED CONFLICTS ===
==========================


BEGIN
DEPOSIT D.t 10 <<<<< D is the coordinator
ABORT



==========================


BEGIN
DEPOSIT D.t 10 <<<<< D is NOT the coordinator
ABORT