import collections

from constants import NODE_IDENTIFIER, NODE_ADDRESS, NODE_PORT, CEND, CRED, CGREEN, CPINK
from multiprocessing.pool import ThreadPool
import multiprocessing as mp
import traceback
from transaction import Transaction

MAX_THREAD_COUNT = mp.cpu_count()
THREAD_POOL = ThreadPool(MAX_THREAD_COUNT)


def parse_config(config_path: str) -> dict:
    nodes = {}
    with open(config_path, 'r') as file:
        for line in file:
            node_info = line.split(" ")
            if len(node_info) == 3:
                nodes[node_info[NODE_IDENTIFIER]] = {
                    NODE_IDENTIFIER: node_info[NODE_IDENTIFIER],
                    NODE_ADDRESS: node_info[NODE_ADDRESS],
                    NODE_PORT: int(node_info[NODE_PORT].rstrip())
                }
    return nodes


def run_in_threads(task_list, **kwargs):
    ttl = int(kwargs.get('timeout') or 120)
    task_map = {}
    for i, (func, params) in enumerate(task_list):
        task_map[i] = THREAD_POOL.apply_async(func, args=params)

    running_tasks = [task_map[i] for i in sorted(task_map.keys())]
    results = []
    for r in running_tasks:
        value = None
        try:
            value = r.get(ttl)
        except Exception as e:
            print(CRED + '[Multithreading] - Failed to get result from thread: {}.{}.{}'
                  .format(e.__class__.__name__, e, traceback.format_exc()) + CEND)
        results.append(value)
    return results


def deliverable(transaction: Transaction, transactions_proposednode: collections.defaultdict, group: dict):
    transaction_id = transaction.transaction_str + "!" + transaction.transaction_time
    nodes_list = transactions_proposednode[transaction_id]
    for node in group:
        if node not in nodes_list:
            return False
    return True


def rprint(msg):
    print(CRED + msg + CEND)


def gprint(msg):
    print(CGREEN + msg + CEND)


def pprint(msg):
    print(CPINK + msg + CEND)
