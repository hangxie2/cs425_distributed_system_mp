from transaction import Transaction


class Message:
    def __init__(self, transaction: Transaction, priority: int, node_num: int, is_final_msg: bool):
        self.transaction = transaction
        # the format changed to priority.node_num
        self.priority = str.format(f"{priority}.{node_num}")
        self.is_final_msg = is_final_msg
        # self.send_time = -1  # will be set when send the message
        self.node_num = node_num

    # for the order of priority queue
    def __lt__(self, other):
        self_priority_info = self.priority.split(".")
        other_priority_info = other.priority.split(".")
        if self_priority_info[0] != other_priority_info[0]:
            return int(self_priority_info[0]) < int(other_priority_info[0])
        else:
            # todo
            return int(self_priority_info[1].split('node')[1]) < int(other_priority_info[1].split('node')[1])
