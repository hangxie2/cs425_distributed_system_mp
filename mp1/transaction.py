import time

from constants import DEPOSIT, TRANSFER


class Transaction:
    def __init__(self, transaction_str: str, my_id: int):
        self.transaction_origin = my_id
        self.transaction_str = transaction_str
        self.transaction_time = time.time()
        self.transaction_id = str(my_id) + "_" + transaction_str + "_" + str(self.transaction_time)
        str_segments = transaction_str.split(" ")

        if transaction_str.startswith(DEPOSIT):
            self.account = str_segments[1]
            self.fund = str_segments[2]
            self.type = DEPOSIT
        elif transaction_str.startswith(TRANSFER):
            self.from_account = str_segments[1]
            self.to_account = str_segments[3]
            self.fund = str_segments[4]
            self.type = TRANSFER
        else:
            print(f"[Transaction] - Cannot identify transaction_str : {transaction_str}")

    def __eq__(self, other_transaction):
        return isinstance(self.__class__, other_transaction) \
            and self.transaction_id == other_transaction.transaction_id

    def __ne__(self, other_transaction):
        return not self.__eq__(other_transaction)
