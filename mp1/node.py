import sys
import time
import threading
from constants import NODE_ADDRESS, NODE_IDENTIFIER, NODE_PORT, CEND, RECONNECT_TIME, CPINK
from utils import parse_config, gprint, rprint, pprint
import socket
import copy
from message import Message
import pickle
from queue import PriorityQueue
from transaction import Transaction
import traceback
from constants import DEPOSIT, TRANSFER
import logging
import os


SERVER = socket.gethostbyname(socket.gethostname())
NODE_ID_TO_IP_MAPPING = {}
IP_TO_NODE_ID_MAPPING = {}
MSG_LENGTH = 1000
TIMEOUT_SECONDS = 5

logger1 = logging.getLogger('1')
logger1.setLevel(logging.INFO)
formatter1 = logging.Formatter('%(message)s')
file_handler1 = logging.FileHandler('logs1.log', mode='w')
file_handler1.setLevel(logging.INFO)
file_handler1.setFormatter(formatter1)
logger1.addHandler(file_handler1)

logger2 = logging.getLogger('2')
logger2.setLevel(logging.INFO)
formatter2 = logging.Formatter('%(message)s')
file_handler2 = logging.FileHandler(f"logs2.log", mode='w')
file_handler2.setLevel(logging.INFO)
file_handler2.setFormatter(formatter2)
logger2.addHandler(file_handler2)

logger3 = logging.getLogger('3')
logger3.setLevel(logging.INFO)
formatter3 = logging.Formatter('%(message)s')
file_handler3 = logging.FileHandler('logs3.log', mode='w')
file_handler3.setLevel(logging.INFO)
file_handler3.setFormatter(formatter3)
logger3.addHandler(file_handler3)


class SelfNode:
    def __init__(self, my_id):
        self.balance = dict()  # {name, amount} the balances collection in this node
        self.proposed_id = 1  # for proposed id from num 1 start
        self.priority_queue = PriorityQueue()  # priority_queue for the message
        self.my_id = my_id  # my node id
        self.receiver_group_socket_node = dict()
        self.sender_group_socket_node = dict()
        self.err_node = {}
        self.start_time = -1
        self.lock = threading.Lock()
        self.transactions = dict()
        self.received_messages = set()  # 格式 transaction-priority-node_num-is_final_msg-transaction_time 组成的string
        self.observed_priority = 1
        self.current_print_time = 1
        self.delivered_messages = set()

    def update_priority(self):
        self.lock.acquire()
        try:
            if self.observed_priority <= self.proposed_id:
                result = self.proposed_id
                self.proposed_id = self.proposed_id + 1
            else:
                result = self.observed_priority + 1
                self.observed_priority = self.observed_priority + 1
        finally:
            self.lock.release()
        return str(result)


def print_pq(pq: PriorityQueue):
    rprint('-------------CURRENT PQ--------------')
    for msg in pq.queue:
        rprint(str((msg.priority, msg.transaction.transaction_str, msg.is_final_msg,
                    ' '.join([proposed_node for
                              proposed_node in self_node.transactions[msg.transaction.transaction_id]]))))
    rprint('-------------------------------------')


def reorder_priority_queue(self_node: SelfNode):
    new_pq = PriorityQueue()
    for ele in self_node.priority_queue.queue:
        new_pq.put(ele)
    self_node.priority_queue = new_pq


def deliver(self_node: SelfNode):
    self_node.lock.acquire()
    reorder_priority_queue(self_node)

    try:
        while self_node.priority_queue.qsize() > 0:
            # print_pq(self_node.priority_queue)
            peek_msg = self_node.priority_queue.queue[0]
            if peek_msg.is_final_msg or \
                    check_agreement(self_node.transactions[peek_msg.transaction.transaction_id],
                                    self_node.receiver_group_socket_node.keys()):

                deliverable_msg = self_node.priority_queue.get()  # get() function also pops the top element

                if deliverable_msg.transaction.transaction_str in self_node.delivered_messages:
                    continue

                self_node.delivered_messages.add(deliverable_msg.transaction.transaction_str)
                # gprint(f'[{self_node.my_id}] - transaction '
                #        f'[{deliverable_msg.transaction.transaction_str}] delivered!')
                calculate_transaction(deliverable_msg, self_node)
            else:
                break
    finally:
        self_node.lock.release()


def calculate_transaction(msg: Message, self_node: SelfNode):
    present_transaction = msg.transaction
    if present_transaction.type == DEPOSIT:
        self_node.balance[present_transaction.account] = \
            self_node.balance.get(present_transaction.account, 0) + int(present_transaction.fund)
        # gprint(f'[{self_node.my_id}] - DEPOSIT {present_transaction.fund} to {present_transaction.account} SUCCESS!')
    elif present_transaction.type == TRANSFER:
        if present_transaction.from_account not in self_node.balance:
            # rprint(f'[{self_node.my_id}] - calculate_transaction() - transfer error: ')
            # rprint(f'   [{self_node.my_id}] - we dont have from_account '
            #        f'{present_transaction.from_account} in our database')
            return
        if self_node.balance[present_transaction.from_account] >= int(present_transaction.fund):
            self_node.balance[present_transaction.from_account] -= int(present_transaction.fund)
            self_node.balance.setdefault(present_transaction.to_account, 0)
            self_node.balance[present_transaction.to_account] += int(present_transaction.fund)
            # gprint(f'[{self_node.my_id}] - TRANSFER {present_transaction.fund} '
            #        f'from {present_transaction.to_account} to '
            #        f'{present_transaction.from_account} SUCCESS!')
        else:
            # rprint(f'[{self_node.my_id}] - calculate_transaction() - transfer error: ')
            # rprint(f'    Current sender {present_transaction.from_account} balance: '
            #        f'{self_node.balance[present_transaction.from_account]},  '
            #        f'but it wants to transfer {present_transaction.fund}')
            return
    else:
        # rprint(f'[{self_node.my_id}] - calculate_transaction() - '
        #        f'Unexpected transaction type {present_transaction.type}')
        return
    logger2.info(f'{msg.priority}|{msg.transaction.transaction_str}|{msg.transaction.transaction_time}|{time.time()}')
    print_balance(self_node)


def print_balance(self_node: SelfNode):
    # pprint(f'-------------CURRENT BALANCE {self_node.current_print_time} --------------')
    print(CPINK + 'BALANCES', end=' ')
    print(' '.join('{}:{}'.format(a, b) for a, b in sorted(self_node.balance.items())) + CEND)
    self_node.current_print_time += 1
    logger1.info(str(self_node.current_print_time) + ': '
                 + ' '.join('{}:{}'.format(a, b) for a, b in sorted(self_node.balance.items())))
    # pprint('----------------------------------------------')


def connect_with_all_nodes(nodes: dict, current_node_id: str, return_values: dict):
    nodes.pop(current_node_id, None)
    connect_info = {}
    # connect with other nodes
    while nodes:
        remaining_nodes = {}
        for node_id, node in nodes.items():
            try:
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                host_ip_address = NODE_ID_TO_IP_MAPPING[node[NODE_IDENTIFIER]]
                address = (host_ip_address, node[NODE_PORT])
                s.connect(address)
                connect_info[node_id] = s
                gprint(f"[Connect] - "
                       f"Connecting to {node[NODE_IDENTIFIER]}: {host_ip_address}:{node[NODE_PORT]} success!")
            except Exception:
                rprint(f"[Connect] - Connection to "
                       f"{node[NODE_IDENTIFIER]}: {host_ip_address}:{node[NODE_PORT]} failed. "
                       f"Reconnecting in {RECONNECT_TIME}s..")
                remaining_nodes[node_id] = node
        nodes = remaining_nodes
        time.sleep(RECONNECT_TIME)
    return_values['connect_info'] = connect_info
    gprint(f"[Connect] - Connect to all nodes success!")


def bind_with_all_nodes(nodes: dict, current_node_id: str, return_values: dict):
    port = nodes.get(current_node_id, {}).get(NODE_PORT, 1234)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    gprint(f"[Bind] - Current attempt bind server {(SERVER, port)}")
    try:
        s.bind((SERVER, port))
    except Exception as e:
        rprint(f"[Bind Error] - Current address already in use: {(SERVER, port)}. "
               f"Please release this address before running this app : )")
        rprint(f"[ERROR] receiving_handler error:{e.__class__.__name__}.{e}.{traceback.format_exc()}")
        os._exit(0)

    s.listen()
    gprint(f"[Bind] - Server is listening on {(SERVER, port)}")

    bind_info = {}
    nodes.pop(current_node_id, None)
    while True:
        conn, addr = s.accept()
        gprint(f"[Bind] - Bind to {addr} success!")
        bind_info[IP_TO_NODE_ID_MAPPING[addr[0]]] = conn
        if len(bind_info) == len(nodes):
            break

    return_values['bind_info'] = bind_info
    gprint(f"[Bind] - Bind to all nodes success!")


def establish_bidirectional_connection(nodes, current_node_id):
    return_values = {}

    thread1 = threading.Thread(target=bind_with_all_nodes, args=[copy.deepcopy(nodes), current_node_id, return_values])
    thread2 = threading.Thread(target=connect_with_all_nodes,
                               args=[copy.deepcopy(nodes), current_node_id, return_values])

    thread1.start()
    thread2.start()
    thread1.join()
    thread2.join()

    return return_values


def map_hostname_to_ip(nodes):
    for node_id, node in nodes.items():
        NODE_ID_TO_IP_MAPPING[node_id] = socket.gethostbyname(node[NODE_ADDRESS])
        IP_TO_NODE_ID_MAPPING[NODE_ID_TO_IP_MAPPING[node_id]] = node_id


def msg_to_msg_id(msg: Message):
    msg_id = msg.transaction.transaction_id + "-" + msg.priority + "-" + str(msg.is_final_msg)
    return msg_id


def receiving_handler(self_node: SelfNode):
    for node_id, _socket in self_node.receiver_group_socket_node.items():
        thread = threading.Thread(target=receive, args=[self_node, node_id, _socket])
        thread.start()
        # gprint(f'[{self_node.my_id}] - receiving_handler thread for {node_id} is running!')


def is_socket_closed(_socket: socket.socket):
    try:
        # this will try to read bytes without blocking and without removing them from buffer
        data = _socket.recv(MSG_LENGTH, socket.MSG_DONTWAIT | socket.MSG_PEEK)
        """
        recv() will never return an empty string unless the connection has been broken. 
        Ref:
        https://stackoverflow.com/questions/16745409/
        what-does-pythons-socket-recv-return-for-non-blocking-sockets-if-no-data-is-r
        """
        if len(data) == 0:
            return True
    except BlockingIOError:
        # socket is open and reading from it would block
        return False
    except ConnectionResetError:
        # socket was closed
        return True
    except Exception as e:
        # unexpected exception
        rprint(f"[ERROR] receiving_handler error:{e.__class__.__name__}.{e}.{traceback.format_exc()}")
        return False


def check_agreement(proposed_node_list, still_alived_node_list):
    return set(still_alived_node_list).issubset(proposed_node_list)


def transaction_exists_in_pq(self_node: SelfNode, msg: Message):
    for temp_msg in self_node.priority_queue.queue:
        if temp_msg.transaction.transaction_id == msg.transaction.transaction_id:
            return True
    return False


def receive(self_node: SelfNode, node_id: str, _socket: socket.socket):
    while True:
        # check whether the socket is still alive
        if is_socket_closed(_socket):
            # rprint(f'[{self_node.my_id}] - {node_id} is disconnected.')
            del self_node.receiver_group_socket_node[node_id]
            # rprint(f'[{self_node.my_id}] - current alive nodes: '
            #        f'{str([node_id for node_id in self_node.receiver_group_socket_node])}')
            return

        # rprint(f'[{self_node.my_id}] - Receiver CHECKPOINT B')

        # check whether the socket is still alive after first check
        try:
            _socket.settimeout(TIMEOUT_SECONDS)
            msg_bytes = _socket.recv(MSG_LENGTH)
            msg = decode_msg(msg_bytes)
            _socket.settimeout(None)
        except socket.timeout:
            # rprint(f'[{self_node.my_id}] - Receiving from {node_id} timeout... This node is dead!')
            del self_node.receiver_group_socket_node[node_id]
            # rprint(f'[{self_node.my_id}] - Current alive nodes: '
            #        f'{str([node_id for node_id in self_node.receiver_group_socket_node])}')
            return
        except Exception:
            # rprint(f"[ERROR] unexpected receive() Error:{e.__class__.__name__}.{e}.{traceback.format_exc()}")
            pass

        # receive 到msg之后，
        msg_id = msg_to_msg_id(msg)
        # gprint(f'[{self_node.my_id}] - Received msg from {node_id}: ')
        # gprint(f'    {msg.transaction.transaction_str}')
        # gprint(f'    {msg_id}')
        # rprint(f'[{self_node.my_id}] - Receiver CHECKPOINT BB')

        if msg_id in self_node.received_messages:
            # 已经收到过这条msg，不用再广播了，并且pass
            continue
            # rprint(f'[{self_node.my_id}] - DUPLICATE msg [{msg_id}]. DROPPED!')
        else:
            # 需要进行广播
            # rprint(f'[{self_node.my_id}] - Receiver CHECKPOINT C')

            multicast(self_node, msg)

            # 更新observed_priority
            self_node.observed_priority = max(int(msg.priority.split('.')[0]), self_node.observed_priority)

            # 进行处理
            self_node.received_messages.add(msg_id)  # 放入received_messages
            if msg.is_final_msg:
                # 带着agreement的,更新在priority queue 中的位置
                # gprint(f'[{self_node.my_id}] - Received a final message! Processed to deliver!')
                for temp_msg in self_node.priority_queue.queue:
                    if temp_msg.transaction.transaction_id == msg.transaction.transaction_id:
                        temp_msg.is_final_msg = True
                        temp_msg.node_num = msg.node_num
                        temp_msg.priority = msg.priority
                        self_node.received_messages.add(msg_to_msg_id(temp_msg))
                        logger3.info(msg.transaction.transaction_str + ' - ' + msg.priority + '           [delivering A]')
                        deliver(self_node)
                        # broadcast
                        multicast(self_node, msg)
                        break
            else:
                # rprint(f'[{self_node.my_id}] - Receiver CHECKPOINT D')
                # msg带的自己的transaction，那就是别的node带着propose priority来了

                # 如果有node之前给该transaction propose过了，直接让它滚蛋 :)
                if msg.priority.split('.')[1] in self_node.transactions.get(msg.transaction.transaction_id, []):
                    continue

                # 更新transaction收到的propose 的 node list的
                self_node.transactions.setdefault(msg.transaction.transaction_id, []).append(msg.node_num)

                # 这里检查现在还连接着的node是不是收到propose的node_list的子集，是的话就说明已经有priority agreement了
                #   为什么是子集?:
                #       假设现在我们有1、2、3、4、5这些nodes，2和4挂了，我们收到propose的node_list是[1, 3, 4, 5]
                #       现在还连接着的node_list是[1, 3, 5], 只要满足连接着的node是收到propose的node_list的子集就可以了，
                #       因为永远等不到4的propose了 :（
                is_agreed = check_agreement(
                    self_node.transactions[msg.transaction.transaction_id],
                    self_node.receiver_group_socket_node.keys())

                logger3.info(msg.transaction.transaction_str + ' - ' + msg.priority + '           [before reorder]')

                if transaction_exists_in_pq(self_node, msg):
                    # rprint(f'[{self_node.my_id}] - Receiver CHECKPOINT F')
                    for temp_msg in self_node.priority_queue.queue:
                        if temp_msg.transaction.transaction_id == msg.transaction.transaction_id:
                            if temp_msg < msg:
                                temp_msg.priority = msg.priority
                                logger3.info(msg.transaction.transaction_str + ' - ' + msg.priority
                                             + f'           [after reorder] - is_agreed: {is_agreed}')

                            temp_msg.is_final_msg = is_agreed
                            if temp_msg.is_final_msg:
                                self_node.received_messages.add(msg_to_msg_id(temp_msg))
                                logger3.info(
                                    msg.transaction.transaction_str + ' - ' + msg.priority + '           [delivering B]')
                                deliver(self_node)
                                multicast(self_node, temp_msg)
                            break
                else:
                    # propose the priority in the current node
                    # rprint(f'[{self_node.my_id}] - Receiver CHECKPOINT G')
                    msg.priority = self_node.update_priority() + '.' + self_node.my_id
                    logger3.info(msg.transaction.transaction_str + ' - ' + msg.priority
                                 + f'           [first propose]')
                    msg.node_num = self_node.my_id
                    self_node.priority_queue.put(msg)
                    self_node.transactions[msg.transaction.transaction_id].append(self_node.my_id)
                    self_node.received_messages.add(msg_to_msg_id(msg))
                    multicast(self_node, msg)

    # rprint(f'[{self_node.my_id} THREAD END] - receive() END')


def encode_msg(msg):
    encoded_msg = pickle.dumps(msg)
    encoded_msg += b' ' * (MSG_LENGTH - len(encoded_msg))
    try:
        pickle.loads(encoded_msg)
    except Exception:
        pprint(f'[{self_node.my_id} ENCODE MSG] - {msg}')
        pprint(f'[{self_node.my_id} ENCODE MSG] - {msg.transaction.transaction_str}')
    return encoded_msg


def decode_msg(msg) -> Message:
    return pickle.loads(msg)


def multicast(self_node: SelfNode, msg: Message):
    # first put into the received message set
    self_node.lock.acquire()
    try:
        msg_id = msg_to_msg_id(msg)
        # 将要发出的message 放到已接收消息的set中
        self_node.received_messages.add(msg_id)
        # 对group 中的点进行广播
        for node_id, each_socket in self_node.sender_group_socket_node.items():
            # this node_id is dead, just continue
            if node_id not in self_node.receiver_group_socket_node.keys():
                continue
            # gprint(f'[{self_node.my_id}] - Multicasting [{msg.transaction.transaction_str}] to [{node_id}] NOW!')
            each_socket.sendall(encode_msg(msg))
            # gprint(f'[{self_node.my_id}] - Multicasting [{msg.transaction.transaction_str}] to [{node_id}] with'
            # f' proposed priority {msg.priority.split(".")[0]} DONE')
    finally:
        self_node.lock.release()


def read_gentx(self_node: SelfNode):
    while True:
        # rprint(f'[{self_node.my_id}] - read_gentx CHECKPOINT A')
        data = input()
        temp_transaction = Transaction(data, self_node.my_id)
        self_node.transactions[temp_transaction.transaction_id] = [self_node.my_id]
        priority = self_node.update_priority()
        # pprint(f'[{self_node.my_id}] - New Transaction [{data}] read!')

        # generate a object could be put in the priority queue
        msg = Message(temp_transaction, priority, self_node.my_id, False)

        logger3.info(msg.transaction.transaction_str + ' - ' + msg.priority)

        self_node.priority_queue.put(msg)  # put the msg into priority_queue

        self_node.transactions.setdefault(temp_transaction.transaction_id, []).append(self_node.my_id)  # 放到transaction 中的列表
        # rprint(f'[{self_node.my_id}] - read_gentx CHECKPOINT B')
        multicast(self_node, msg)
        # rprint(f'[{self_node.my_id}] - read_gentx CHECKPOINT C')

    # rprint(f'[{self_node.my_id} THREAD END] - read_gentx() END')


if __name__ == '__main__':
    current_node_id = sys.argv[1]
    config_path = sys.argv[2]
    nodes = parse_config(config_path)
    map_hostname_to_ip(nodes)
    self_node = SelfNode(current_node_id)

    '''
        return_values = {
            # sockets used for sending data
            # it will NOT include the socket for the current node itself
            'connect_info': {
                'node2': <socket1>,
                'node3': <socket2>,
                ...
            },
            # sockets used for receiving data
            # it will NOT include the socket for the current node itself
            'bind_info': {
                'node2': <socket1>,
                'node3': <socket2>,
                ...
            },
        }
    '''
    return_values = establish_bidirectional_connection(nodes, current_node_id)
    # 将group 进行一个初始化
    for key, value in return_values.items():
        for node, sok in value.items():
            if key == 'connect_info':
                self_node.receiver_group_socket_node[node] = sok
            else:
                self_node.sender_group_socket_node[node] = sok

    thread1 = threading.Thread(target=receiving_handler, args=[self_node])
    thread2 = threading.Thread(target=read_gentx, args=[self_node])
    thread1.start()
    thread2.start()
