import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pathlib


def draw_graph():
    for path in pathlib.Path("./mp1_graph_data").iterdir():
        if 'case' in str(path):
            with open(str(path) + "/all_in_one.log", "r") as file:
                time_processed_per_msg = []
                for line in file:
                    line_info = line.split('|')
                    end_time = float(line_info[3])
                    start_time = float(line_info[2])
                    time_processed_per_msg.append(end_time - start_time)

            time_processed_per_msg.sort()
            print(time_processed_per_msg)
            _sum = sum(time_processed_per_msg)
            print(_sum)
            print(np.cumsum(time_processed_per_msg) / _sum)
            time_processed_per_msg = (np.cumsum(time_processed_per_msg) / _sum).tolist()

            step = 0.05
            indices = np.arange(0, 1 + step, step)
            cdf = pd.DataFrame({'dummy': time_processed_per_msg})['dummy'].quantile(indices)
            plt.title(str(path).split('mp1_graph_data/')[1])
            plt.xlabel('Processed Time (s)')
            plt.ylabel('Cumulative Probability (%)')

            plt.plot(cdf, indices, linewidth=1, label='processed time', color='blue')
            plt.savefig(str(path) + '/graph.png')
            plt.clf()


def prepare_all_in_one_log():
    for path in pathlib.Path("./mp1_graph_data").iterdir():
        if 'case' in str(path):
            # print(path)
            output_file = open(str(path) + "/all_in_one.log", "w")
            output_list = []
            for log in pathlib.Path(str(path)).iterdir():
                cur_line = 0
                with open(log, 'r') as file:
                    for line in file:
                        info = line.split('|')
                        if len(output_list) > cur_line:
                            output_list[cur_line][3] = max(float(output_list[cur_line][3]), float(info[3]))
                            cur_line += 1
                        else:
                            output_list.append([info[0], info[1], info[2], info[3]])
                            cur_line += 1

            for log_with_latest_time in output_list:
                output_file.write(f'{log_with_latest_time[0]}|{log_with_latest_time[1]}'
                                  f'|{log_with_latest_time[2]}|{log_with_latest_time[3]}\n')
            output_file.close()


if __name__ == '__main__':
    # prepare_all_in_one_log()
    draw_graph()
