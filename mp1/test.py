from message import Message
from transaction import Transaction
from queue import PriorityQueue


def print_pq(pq: PriorityQueue):
    print('-------------CURRENT PQ--------------')
    for msg in pq.queue:
        print(str((msg.priority, msg.transaction.transaction_str, msg.is_final_msg)))
    print('-------------------------------------')


if __name__ == "__main__":
    msg1 = Message(Transaction('DEPOSIT y 58', 'node1'), 1, 'node3', False)
    msg2 = Message(Transaction('DEPOSIT y 58', 'node1'), 1, 'node1', False)
    msg3 = Message(Transaction('DEPOSIT y 58', 'node1'), 2, 'node3', False)

    q = PriorityQueue()
    q.put(msg1)
    q.put(msg2)
    q.put(msg3)

    print_pq(q)

    msg2.priority = '5.node4'

    print_pq(q)

    new_pq = PriorityQueue()
    for ele in q.queue:
        new_pq.put(ele)
    q = new_pq
    print_pq(q)
    while q.qsize() > 0:
        print(q.get().priority)
